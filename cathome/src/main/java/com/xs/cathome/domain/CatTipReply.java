package com.xs.cathome.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 打赏评论对象 cat_tip_reply
 * 
 * @author ruoyi
 * @date 2023-03-14
 */
public class CatTipReply extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 用户id */
    private String userId;

    /** 评论文章id */
    @Excel(name = "评论文章id")
    private String articleId;

    /** 评论内容 */
    @Excel(name = "评论内容")
    private String article;

    /** 点赞数 */
    @Excel(name = "点赞数")
    private Long numLike;

    /** 上级评论者id */
    @Excel(name = "上级评论者id")
    private String articlePid;

    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }
    public void setArticleId(String articleId) 
    {
        this.articleId = articleId;
    }

    public String getArticleId() 
    {
        return articleId;
    }
    public void setArticle(String article) 
    {
        this.article = article;
    }

    public String getArticle() 
    {
        return article;
    }
    public void setNumLike(Long numLike) 
    {
        this.numLike = numLike;
    }

    public Long getNumLike() 
    {
        return numLike;
    }
    public void setArticlePid(String articlePid) 
    {
        this.articlePid = articlePid;
    }

    public String getArticlePid() 
    {
        return articlePid;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("userId", getUserId())
            .append("articleId", getArticleId())
            .append("article", getArticle())
            .append("numLike", getNumLike())
            .append("articlePid", getArticlePid())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
