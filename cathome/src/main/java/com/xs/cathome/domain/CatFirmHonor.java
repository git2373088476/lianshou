package com.xs.cathome.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 公司荣誉表对象 cat_firm_honor
 * 
 * @author ruoyi
 * @date 2023-03-15
 */
public class CatFirmHonor extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 公司荣誉id */
    private String honorId;

    /** 公司id */
    @Excel(name = "公司id")
    private String firmId;

    /** 用户员工id */
    @Excel(name = "用户员工id")
    private String userId;

    public void setHonorId(String honorId) 
    {
        this.honorId = honorId;
    }

    public String getHonorId() 
    {
        return honorId;
    }
    public void setFirmId(String firmId) 
    {
        this.firmId = firmId;
    }

    public String getFirmId() 
    {
        return firmId;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("honorId", getHonorId())
            .append("firmId", getFirmId())
            .append("userId", getUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
