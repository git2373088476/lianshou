package com.xs.cathome.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 公司表对象 cat_firm
 * 
 * @author ruoyi
 * @date 2023-03-15
 */
public class CatFirm extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 公司id */
    private String firmId;

    /** 公司名称 */
    @Excel(name = "公司名称")
    private String firmName;

    public void setFirmId(String firmId) 
    {
        this.firmId = firmId;
    }

    public String getFirmId() 
    {
        return firmId;
    }
    public void setFirmName(String firmName) 
    {
        this.firmName = firmName;
    }

    public String getFirmName() 
    {
        return firmName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("firmId", getFirmId())
            .append("firmName", getFirmName())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
