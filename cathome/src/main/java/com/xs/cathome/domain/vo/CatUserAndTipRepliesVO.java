package com.xs.cathome.domain.vo;

import com.ruoyi.common.annotation.Excel;

import java.io.Serializable;

/**
 * 用户和评论vo 包括用户id、头像、名字、文章id、评论内容、点赞数、上级评论者id
 */

public class CatUserAndTipRepliesVO implements Serializable {
    @Override
    public String toString() {
        return "CatUserAndTipRepliesVO{" +
                "userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", userLogo='" + userLogo + '\'' +
                ", articleId='" + articleId + '\'' +
                ", article='" + article + '\'' +
                ", numLike=" + numLike +
                ", articlePid='" + articlePid + '\'' +
                '}';
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserLogo() {
        return userLogo;
    }

    public void setUserLogo(String userLogo) {
        this.userLogo = userLogo;
    }

    public String getArticleId() {
        return articleId;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public Long getNumLike() {
        return numLike;
    }

    public void setNumLike(Long numLike) {
        this.numLike = numLike;
    }

    public String getArticlePid() {
        return articlePid;
    }

    public void setArticlePid(String articlePid) {
        this.articlePid = articlePid;
    }

    public CatUserAndTipRepliesVO() {
    }

    public CatUserAndTipRepliesVO(String userId, String userName, String userLogo, String articleId, String article, Long numLike, String articlePid) {
        this.userId = userId;
        this.userName = userName;
        this.userLogo = userLogo;
        this.articleId = articleId;
        this.article = article;
        this.numLike = numLike;
        this.articlePid = articlePid;
    }

    /** id */
    private String userId;

    /** 昵称 */
    private String userName;

    /** 头像 */
    private String userLogo;

    /** 评论文章id */
    private String articleId;

    /** 评论内容 */
    private String article;

    /** 点赞数 */
    private Long numLike;

    /** 上级评论者id */
    private String articlePid;

}
