package com.xs.cathome.domain.vo;

import com.xs.cathome.domain.CatTipArticle;

import java.io.Serializable;

public class CatinfoVO implements Serializable {
    private String inforTypeId;
    private CatTipArticle catTipArticle;

    public String getInforTypeId() {
        return inforTypeId;
    }

    public void setInforTypeId(String inforTypeId) {
        this.inforTypeId = inforTypeId;
    }

    public CatTipArticle getCatTipArticle() {
        return catTipArticle;
    }

    public void setCatTipArticle(CatTipArticle catTipArticle) {
        this.catTipArticle = catTipArticle;
    }

    public CatinfoVO() {
    }

    public CatinfoVO(String inforTypeId, CatTipArticle catTipArticle) {
        this.inforTypeId = inforTypeId;
        this.catTipArticle = catTipArticle;
    }
}
