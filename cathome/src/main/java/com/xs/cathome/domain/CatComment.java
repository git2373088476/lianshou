package com.xs.cathome.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 评论对象 cat_comment
 * 
 * @author ruoyi
 * @date 2023-03-14
 */
public class CatComment extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 评论id */
    private String commentId;

    /** 文章id */
    @Excel(name = "文章id")
    private String articleId;

    /** 评论内容
 */
    @Excel(name = "评论内容 ")
    private String commentText;

    /** 评论图片id */
    @Excel(name = "评论图片id")
    private String commentImgid;

    /** 评论用户 */
    @Excel(name = "评论用户")
    private String commentUserid;

    /** 父级评论 */
    @Excel(name = "父级评论")
    private String parentId;

    public void setCommentId(String commentId) 
    {
        this.commentId = commentId;
    }

    public String getCommentId() 
    {
        return commentId;
    }
    public void setArticleId(String articleId) 
    {
        this.articleId = articleId;
    }

    public String getArticleId() 
    {
        return articleId;
    }
    public void setCommentText(String commentText) 
    {
        this.commentText = commentText;
    }

    public String getCommentText() 
    {
        return commentText;
    }
    public void setCommentImgid(String commentImgid) 
    {
        this.commentImgid = commentImgid;
    }

    public String getCommentImgid() 
    {
        return commentImgid;
    }
    public void setCommentUserid(String commentUserid) 
    {
        this.commentUserid = commentUserid;
    }

    public String getCommentUserid() 
    {
        return commentUserid;
    }
    public void setParentId(String parentId) 
    {
        this.parentId = parentId;
    }

    public String getParentId() 
    {
        return parentId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("commentId", getCommentId())
            .append("articleId", getArticleId())
            .append("commentText", getCommentText())
            .append("commentImgid", getCommentImgid())
            .append("commentUserid", getCommentUserid())
            .append("parentId", getParentId())
            .toString();
    }
}
