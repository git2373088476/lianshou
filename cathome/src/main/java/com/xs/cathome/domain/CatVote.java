package com.xs.cathome.domain;

import java.util.List;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * Catvote对象 cat_vote
 * 
 * @author ruoyi
 * @date 2023-03-17
 */
public class CatVote extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private String voteId;

    /**  */
    @Excel(name = "")
    private String catUserId;

    /**  */
    @Excel(name = "")
    private String title;

    /**  */
    @Excel(name = "")
    private String description;

    /**  */
    @Excel(name = "")
    private String chooseFlag;

    /** 文章图片 评论图片信息 */
    private List<CatImg> catImgList;

    public void setVoteId(String voteId) 
    {
        this.voteId = voteId;
    }

    public String getVoteId() 
    {
        return voteId;
    }
    public void setCatUserId(String catUserId) 
    {
        this.catUserId = catUserId;
    }

    public String getCatUserId() 
    {
        return catUserId;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setDescription(String description) 
    {
        this.description = description;
    }

    public String getDescription() 
    {
        return description;
    }
    public void setChooseFlag(String chooseFlag) 
    {
        this.chooseFlag = chooseFlag;
    }

    public String getChooseFlag() 
    {
        return chooseFlag;
    }

    public List<CatImg> getCatImgList()
    {
        return catImgList;
    }

    public void setCatImgList(List<CatImg> catImgList)
    {
        this.catImgList = catImgList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("voteId", getVoteId())
            .append("catUserId", getCatUserId())
            .append("title", getTitle())
            .append("description", getDescription())
            .append("createTime", getCreateTime())
            .append("chooseFlag", getChooseFlag())
            .append("catImgList", getCatImgList())
            .toString();
    }
}
