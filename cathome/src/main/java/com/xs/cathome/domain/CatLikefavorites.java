package com.xs.cathome.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 点赞收藏对象 cat_likefavorites
 * 
 * @author ruoyi
 * @date 2023-03-14
 */
public class CatLikefavorites extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private String userId;

    /** 文章id */
    @Excel(name = "文章id")
    private String articleId;

    /** 点赞 */
    @Excel(name = "点赞")
    private Long like;

    /** 收藏 */
    @Excel(name = "收藏")
    private Long collect;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }
    public void setArticleId(String articleId) 
    {
        this.articleId = articleId;
    }

    public String getArticleId() 
    {
        return articleId;
    }
    public void setLike(Long like) 
    {
        this.like = like;
    }

    public Long getLike() 
    {
        return like;
    }
    public void setCollect(Long collect) 
    {
        this.collect = collect;
    }

    public Long getCollect() 
    {
        return collect;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("articleId", getArticleId())
            .append("like", getLike())
            .append("collect", getCollect())
            .toString();
    }
}
