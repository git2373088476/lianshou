package com.xs.cathome.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 cat_user
 * 
 * @author ruoyi
 * @date 2023-03-13
 */
public class CatUser extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private String userId;

    /** 手机号 */
    @Excel(name = "手机号")
    private String phone;

    /** 密码 */
    @Excel(name = "密码")
    private String password;

    /** 昵称 */
    @Excel(name = "昵称")
    private String userName;

    /** 头像 */
    @Excel(name = "头像")
    private String userLogo;

    /** 个签 */
    @Excel(name = "个签")
    private String signature;

    /** 消息提示状态 */
    @Excel(name = "消息提示状态")
    private String message;

    /** 积分 */
    @Excel(name = "积分")
    private String integral;

    /** 登录时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "登录时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date loginDate;

    /** 最后登录IP */
    @Excel(name = "最后登录IP")
    private String loginIp;

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getUserId()
    {
        return userId;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setPassword(String password) 
    {
        this.password = password;
    }

    public String getPassword() 
    {
        return password;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setUserLogo(String userLogo) 
    {
        this.userLogo = userLogo;
    }

    public String getUserLogo() 
    {
        return userLogo;
    }
    public void setSignature(String signature) 
    {
        this.signature = signature;
    }

    public String getSignature() 
    {
        return signature;
    }
    public void setMessage(String message) 
    {
        this.message = message;
    }

    public String getMessage() 
    {
        return message;
    }
    public void setIntegral(String integral) 
    {
        this.integral = integral;
    }

    public String getIntegral() 
    {
        return integral;
    }
    public void setLoginDate(Date loginDate) 
    {
        this.loginDate = loginDate;
    }

    public Date getLoginDate() 
    {
        return loginDate;
    }
    public void setLoginIp(String loginIp) 
    {
        this.loginIp = loginIp;
    }

    public String getLoginIp() 
    {
        return loginIp;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("userId", getUserId())
            .append("phone", getPhone())
            .append("password", getPassword())
            .append("userName", getUserName())
            .append("userLogo", getUserLogo())
            .append("signature", getSignature())
            .append("message", getMessage())
            .append("integral", getIntegral())
            .append("createTime", getCreateTime())
            .append("loginDate", getLoginDate())
            .append("loginIp", getLoginIp())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
