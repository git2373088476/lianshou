package com.xs.cathome.domain.vo;

import com.xs.cathome.domain.CatFleaMarket;
import com.xs.cathome.domain.CatTipArticle;

import java.io.Serializable;

public class CatFleaMarketVO implements Serializable {
    private CatFleaMarket catFleaMarket;
    private CatTipArticle catTipArticle;

    @Override
    public String toString() {
        return "CatFleaMarketVO{" +
                "catFleaMarket=" + catFleaMarket +
                ", catTipArticle=" + catTipArticle +
                '}';
    }

    public CatFleaMarket getCatFleaMarket() {
        return catFleaMarket;
    }

    public void setCatFleaMarket(CatFleaMarket catFleaMarket) {
        this.catFleaMarket = catFleaMarket;
    }

    public CatTipArticle getCatTipArticle() {
        return catTipArticle;
    }

    public void setCatTipArticle(CatTipArticle catTipArticle) {
        this.catTipArticle = catTipArticle;
    }

    public CatFleaMarketVO(CatFleaMarket catFleaMarket, CatTipArticle catTipArticle) {
        this.catFleaMarket = catFleaMarket;
        this.catTipArticle = catTipArticle;
    }

    public CatFleaMarketVO() {
    }
}
