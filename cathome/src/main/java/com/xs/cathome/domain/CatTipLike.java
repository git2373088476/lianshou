package com.xs.cathome.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 打赏点赞对象 cat_tip_like
 * 
 * @author ruoyi
 * @date 2023-03-14
 */
public class CatTipLike extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 打赏点赞id */
    private String likeId;

    /** 点赞用户id */
    @Excel(name = "点赞用户id")
    private String userId;

    /** 点赞文章id */
    @Excel(name = "点赞文章id")
    private String articleId;

    public void setLikeId(String likeId) 
    {
        this.likeId = likeId;
    }

    public String getLikeId() 
    {
        return likeId;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }
    public void setArticleId(String articleId) 
    {
        this.articleId = articleId;
    }

    public String getArticleId() 
    {
        return articleId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("likeId", getLikeId())
            .append("userId", getUserId())
            .append("articleId", getArticleId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
