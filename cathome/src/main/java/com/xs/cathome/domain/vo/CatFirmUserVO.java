package com.xs.cathome.domain.vo;

import com.ruoyi.common.annotation.Excel;
import com.xs.cathome.domain.CatTipArticle;

import java.io.Serializable;

/**
 * 公司、员工表
 */

public class CatFirmUserVO implements Serializable {
    /** 公司荣誉id */
    private String honorId;

    /** 公司id */
    private String firmId;

    /** 公司名称 */
    private String firmName;

    /** 用户员工id */
    private String userId;

    /** 昵称 */
    private String userName;

    /** 头像 */
    private String userLogo;

    /** 发布的内容 */
    private CatTipArticle catTipArticle;

    public String getHonorId() {
        return honorId;
    }

    public void setHonorId(String honorId) {
        this.honorId = honorId;
    }

    public String getFirmId() {
        return firmId;
    }

    public void setFirmId(String firmId) {
        this.firmId = firmId;
    }

    public String getFirmName() {
        return firmName;
    }

    public void setFirmName(String firmName) {
        this.firmName = firmName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserLogo() {
        return userLogo;
    }

    public void setUserLogo(String userLogo) {
        this.userLogo = userLogo;
    }

    public CatTipArticle getCatTipArticle() {
        return catTipArticle;
    }

    public void setCatTipArticle(CatTipArticle catTipArticle) {
        this.catTipArticle = catTipArticle;
    }

    public CatFirmUserVO() {
    }

    public CatFirmUserVO(String honorId, String firmId, String firmName, String userId, String userName, String userLogo, CatTipArticle catTipArticle) {
        this.honorId = honorId;
        this.firmId = firmId;
        this.firmName = firmName;
        this.userId = userId;
        this.userName = userName;
        this.userLogo = userLogo;
        this.catTipArticle = catTipArticle;
    }
}
