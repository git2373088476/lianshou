package com.xs.cathome.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 公司荣誉和评论对象 cat_firm_honor_article
 * 
 * @author ruoyi
 * @date 2023-03-15
 */
public class CatFirmHonorArticle extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 公司荣誉和发表id */
    private String artHonorId;

    /** 公司荣誉id */
    @Excel(name = "公司荣誉id")
    private String honorId;

    /** 评论id */
    @Excel(name = "评论id")
    private String articleId;

    public void setArtHonorId(String artHonorId) 
    {
        this.artHonorId = artHonorId;
    }

    public String getArtHonorId() 
    {
        return artHonorId;
    }
    public void setHonorId(String honorId) 
    {
        this.honorId = honorId;
    }

    public String getHonorId() 
    {
        return honorId;
    }
    public void setArticleId(String articleId) 
    {
        this.articleId = articleId;
    }

    public String getArticleId() 
    {
        return articleId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("artHonorId", getArtHonorId())
            .append("honorId", getHonorId())
            .append("articleId", getArticleId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
