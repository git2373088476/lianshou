package com.xs.cathome.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 跳蚤市场对象 cat_flea_market
 * 
 * @author ruoyi
 * @date 2023-03-16
 */
public class CatFleaMarket extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 跳蚤市场id */
    private String marketId;

    /** 用户id */
    @Excel(name = "用户id")
    private String userId;

    /** 价格 */
    @Excel(name = "价格")
    private Long price;

    /** 商品信息id */
    @Excel(name = "商品信息id")
    private String articleId;

    /** 类型 */
    @Excel(name = "类型")
    private String type;

    /** 是否匿名 */
    @Excel(name = "是否匿名")
    private Long anonymity;

    public void setMarketId(String marketId) 
    {
        this.marketId = marketId;
    }

    public String getMarketId() 
    {
        return marketId;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }
    public void setPrice(Long price) 
    {
        this.price = price;
    }

    public Long getPrice() 
    {
        return price;
    }
    public void setArticleId(String articleId) 
    {
        this.articleId = articleId;
    }

    public String getArticleId() 
    {
        return articleId;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }
    public void setAnonymity(Long anonymity) 
    {
        this.anonymity = anonymity;
    }

    public Long getAnonymity() 
    {
        return anonymity;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("marketId", getMarketId())
            .append("userId", getUserId())
            .append("price", getPrice())
            .append("articleId", getArticleId())
            .append("type", getType())
            .append("anonymity", getAnonymity())
            .toString();
    }
}
