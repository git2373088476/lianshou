package com.xs.cathome.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 文章图片 评论图片对象 cat_img
 * 
 * @author ruoyi
 * @date 2023-03-17
 */
public class CatImg extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 图片id */
    @Excel(name = "图片id")
    private String imgId;

    /** 文章id */
    @Excel(name = "文章id")
    private String articleId;

    /** 图片保存名称 */
    @Excel(name = "图片保存名称")
    private String imgName;


    /** 判断是否为评论图片 */
    @Excel(name = "判断是否为评论图片")
    private String commentId;

    public void setImgId(String imgId) 
    {
        this.imgId = imgId;
    }

    public String getImgId() 
    {
        return imgId;
    }
    public void setArticleId(String articleId) 
    {
        this.articleId = articleId;
    }

    public String getArticleId() 
    {
        return articleId;
    }
    public void setImgName(String imgName) 
    {
        this.imgName = imgName;
    }

    public String getImgName() 
    {
        return imgName;
    }
    public void setCommentId(String commentId) 
    {
        this.commentId = commentId;
    }

    public String getCommentId() 
    {
        return commentId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("imgId", getImgId())
            .append("articleId", getArticleId())
            .append("imgName", getImgName())
            .append("commentId", getCommentId())
            .toString();
    }
}
