package com.xs.cathome.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 打赏发布表对象 cat_tip_article
 * 
 * @author ruoyi
 * @date 2023-03-14
 */
public class CatTipArticle extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 打赏发布的文章id */
    private String articleId;

    /** 打赏用户id */
    @Excel(name = "打赏用户id")
    private String userId;

    /** 发布评论 */
    @Excel(name = "发布评论")
    private String comments;

    /** 发布图片 */
    @Excel(name = "发布图片")
    private String img;

    /** IP地址 */
    @Excel(name = "IP地址")
    private String ip;

    /** 点赞数 */
    @Excel(name = "点赞数")
    private Long numLike = 0L;

    /** 浏览数 */
    @Excel(name = "浏览数")
    private Long numLook = 0L;

    /** 收藏数 */
    @Excel(name = "收藏数")
    private Long numCollect = 0L;

    public void setArticleId(String articleId) 
    {
        this.articleId = articleId;
    }

    public String getArticleId() 
    {
        return articleId;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }
    public void setComments(String comments) 
    {
        this.comments = comments;
    }

    public String getComments() 
    {
        return comments;
    }
    public void setImg(String img) 
    {
        this.img = img;
    }

    public String getImg() 
    {
        return img;
    }
    public void setIp(String ip) 
    {
        this.ip = ip;
    }

    public String getIp() 
    {
        return ip;
    }
    public void setNumLike(Long numLike) 
    {
        this.numLike = numLike;
    }

    public Long getNumLike() 
    {
        return numLike;
    }
    public void setNumLook(Long numLook) 
    {
        this.numLook = numLook;
    }

    public Long getNumLook() 
    {
        return numLook;
    }
    public void setNumCollect(Long numCollect) 
    {
        this.numCollect = numCollect;
    }

    public Long getNumCollect() 
    {
        return numCollect;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("articleId", getArticleId())
            .append("userId", getUserId())
            .append("comments", getComments())
            .append("img", getImg())
            .append("ip", getIp())
            .append("numLike", getNumLike())
            .append("numLook", getNumLook())
            .append("numCollect", getNumCollect())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
