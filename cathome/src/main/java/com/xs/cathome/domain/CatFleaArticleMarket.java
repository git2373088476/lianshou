package com.xs.cathome.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 跳蚤信息关联表对象 cat_flea_article_market
 * 
 * @author ruoyi
 * @date 2023-03-16
 */
public class CatFleaArticleMarket extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 跳蚤和发布的信息关联id */
    private String artMarId;

    /** 跳蚤id */
    @Excel(name = "跳蚤id")
    private String marketId;

    /** 发布的信息id */
    @Excel(name = "发布的信息id")
    private String articleId;

    public void setArtMarId(String artMarId) 
    {
        this.artMarId = artMarId;
    }

    public String getArtMarId() 
    {
        return artMarId;
    }
    public void setMarketId(String marketId) 
    {
        this.marketId = marketId;
    }

    public String getMarketId() 
    {
        return marketId;
    }
    public void setArticleId(String articleId) 
    {
        this.articleId = articleId;
    }

    public String getArticleId() 
    {
        return articleId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("artMarId", getArtMarId())
            .append("marketId", getMarketId())
            .append("articleId", getArticleId())
            .toString();
    }
}
