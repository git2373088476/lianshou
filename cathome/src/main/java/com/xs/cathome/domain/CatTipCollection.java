package com.xs.cathome.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 打赏收藏对象 cat_tip_collection
 * 
 * @author ruoyi
 * @date 2023-03-14
 */
public class CatTipCollection extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 打赏收藏id */
    private String collectionId;

    /** 打赏用户id */
    @Excel(name = "打赏用户id")
    private String userId;

    /** 打赏文章id */
    @Excel(name = "打赏文章id")
    private String articleId;

    public void setCollectionId(String collectionId) 
    {
        this.collectionId = collectionId;
    }

    public String getCollectionId() 
    {
        return collectionId;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }
    public void setArticleId(String articleId) 
    {
        this.articleId = articleId;
    }

    public String getArticleId() 
    {
        return articleId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("collectionId", getCollectionId())
            .append("userId", getUserId())
            .append("articleId", getArticleId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
