package com.xs.cathome.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 资讯关系表对象 cat_inforlist
 * 
 * @author ruoyi
 * @date 2023-03-15
 */
public class CatInforlist extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 资讯表id */
    private String inforlistId;

    /** 资讯类型id */
    @Excel(name = "资讯类型id")
    private String inforTypeId;

    /** 资讯内容id */
    @Excel(name = "资讯内容id")
    private String articleId;

    public void setInforlistId(String inforlistId) 
    {
        this.inforlistId = inforlistId;
    }

    public String getInforlistId() 
    {
        return inforlistId;
    }
    public void setInforTypeId(String inforTypeId) 
    {
        this.inforTypeId = inforTypeId;
    }

    public String getInforTypeId() 
    {
        return inforTypeId;
    }
    public void setArticleId(String articleId) 
    {
        this.articleId = articleId;
    }

    public String getArticleId() 
    {
        return articleId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("inforlistId", getInforlistId())
            .append("inforTypeId", getInforTypeId())
            .append("articleId", getArticleId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
