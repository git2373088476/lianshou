package com.xs.cathome.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 奖品对象 cat_prize
 * 
 * @author ruoyi
 * @date 2023-03-14
 */
public class CatPrize extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 文章id */
    private String articleId;

    /** 发布用户id */
    @Excel(name = "发布用户id")
    private String userId;

    /** 文章内容 */
    @Excel(name = "文章内容")
    private String content;

    /** 图片id */
    @Excel(name = "图片id")
    private String imgId;

    /** 发布位置 */
    @Excel(name = "发布位置")
    private String place;

    /** 浏览量 */
    @Excel(name = "浏览量")
    private Long browse;

    /** 评论量 */
    @Excel(name = "评论量")
    private Long comment;

    /** 点赞量 */
    @Excel(name = "点赞量")
    private Long like;

    public void setArticleId(String articleId) 
    {
        this.articleId = articleId;
    }

    public String getArticleId() 
    {
        return articleId;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setImgId(String imgId) 
    {
        this.imgId = imgId;
    }

    public String getImgId() 
    {
        return imgId;
    }
    public void setPlace(String place) 
    {
        this.place = place;
    }

    public String getPlace() 
    {
        return place;
    }
    public void setBrowse(Long browse) 
    {
        this.browse = browse;
    }

    public Long getBrowse() 
    {
        return browse;
    }
    public void setComment(Long comment) 
    {
        this.comment = comment;
    }

    public Long getComment() 
    {
        return comment;
    }
    public void setLike(Long like) 
    {
        this.like = like;
    }

    public Long getLike() 
    {
        return like;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("articleId", getArticleId())
            .append("userId", getUserId())
            .append("content", getContent())
            .append("imgId", getImgId())
            .append("place", getPlace())
            .append("browse", getBrowse())
            .append("comment", getComment())
            .append("like", getLike())
            .toString();
    }
}
