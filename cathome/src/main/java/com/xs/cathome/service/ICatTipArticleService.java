package com.xs.cathome.service;

import java.util.List;
import com.xs.cathome.domain.CatTipArticle;

/**
 * 打赏发布表Service接口
 * 
 * @author ruoyi
 * @date 2023-03-14
 */
public interface ICatTipArticleService 
{
    /**
     * 查询打赏发布表
     * 
     * @param articleId 打赏发布表主键
     * @return 打赏发布表
     */
    public CatTipArticle selectCatTipArticleByArticleId(String articleId);

    /**
     * 查询打赏发布表列表
     * 
     * @param catTipArticle 打赏发布表
     * @return 打赏发布表集合
     */
    public List<CatTipArticle> selectCatTipArticleList(CatTipArticle catTipArticle);

    /**
     * 新增打赏发布表
     * 
     * @param catTipArticle 打赏发布表
     * @return 结果
     */
    public int insertCatTipArticle(CatTipArticle catTipArticle);

    public String insertCatTipArticleReturnID(CatTipArticle catTipArticle);

    /**
     * 修改打赏发布表
     * 
     * @param catTipArticle 打赏发布表
     * @return 结果
     */
    public int updateCatTipArticle(CatTipArticle catTipArticle);

    /**
     * 批量删除打赏发布表
     * 
     * @param articleIds 需要删除的打赏发布表主键集合
     * @return 结果
     */
    public int deleteCatTipArticleByArticleIds(String[] articleIds);

    /**
     * 删除打赏发布表信息
     * 
     * @param articleId 打赏发布表主键
     * @return 结果
     */
    public int deleteCatTipArticleByArticleId(String articleId);

    List<CatTipArticle> selectCatTipArticleList(String status);

    CatTipArticle selectCatTipArticleByArticleIdAndUserId(String articleId, String userId);
}
