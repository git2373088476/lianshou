package com.xs.cathome.service;

import java.util.List;
import com.xs.cathome.domain.CatImg;

/**
 * 文章图片 评论图片Service接口
 * 
 * @author ruoyi
 * @date 2023-03-14
 */
public interface ICatImgService 
{
    /**
     * 查询文章图片 评论图片
     * 
     * @param imgId 文章图片 评论图片主键
     * @return 文章图片 评论图片
     */
    public CatImg selectCatImgByImgId(String imgId);

    /**
     * 查询文章图片 评论图片列表
     * 
     * @param catImg 文章图片 评论图片
     * @return 文章图片 评论图片集合
     */
    public List<CatImg> selectCatImgList(CatImg catImg);

    /**
     * 新增文章图片 评论图片
     * 
     * @param catImg 文章图片 评论图片
     * @return 结果
     */
    public int insertCatImg(CatImg catImg);

    /**
     * 修改文章图片 评论图片
     * 
     * @param catImg 文章图片 评论图片
     * @return 结果
     */
    public int updateCatImg(CatImg catImg);

    /**
     * 批量删除文章图片 评论图片
     * 
     * @param imgIds 需要删除的文章图片 评论图片主键集合
     * @return 结果
     */
    public int deleteCatImgByImgIds(String[] imgIds);

    /**
     * 删除文章图片 评论图片信息
     * 
     * @param imgId 文章图片 评论图片主键
     * @return 结果
     */
    public int deleteCatImgByImgId(String imgId);
}
