package com.xs.cathome.service.impl;

import java.util.List;

import com.xs.cathome.domain.CatUserBlacklist;
import com.xs.cathome.mapper.CatUserBlacklistMapper;
import com.xs.cathome.service.ICatUserBlacklistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-13
 */
@Service
public class CatUserBlacklistServiceImpl implements ICatUserBlacklistService
{
    @Autowired
    private CatUserBlacklistMapper catUserBlacklistMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public CatUserBlacklist selectCatUserBlacklistById(Long id)
    {
        return catUserBlacklistMapper.selectCatUserBlacklistById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param catUserBlacklist 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<CatUserBlacklist> selectCatUserBlacklistList(CatUserBlacklist catUserBlacklist)
    {
        return catUserBlacklistMapper.selectCatUserBlacklistList(catUserBlacklist);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param catUserBlacklist 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertCatUserBlacklist(CatUserBlacklist catUserBlacklist)
    {
        return catUserBlacklistMapper.insertCatUserBlacklist(catUserBlacklist);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param catUserBlacklist 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateCatUserBlacklist(CatUserBlacklist catUserBlacklist)
    {
        return catUserBlacklistMapper.updateCatUserBlacklist(catUserBlacklist);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteCatUserBlacklistByIds(Long[] ids)
    {
        return catUserBlacklistMapper.deleteCatUserBlacklistByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteCatUserBlacklistById(Long id)
    {
        return catUserBlacklistMapper.deleteCatUserBlacklistById(id);
    }
}
