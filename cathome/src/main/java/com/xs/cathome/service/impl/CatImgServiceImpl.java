package com.xs.cathome.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xs.cathome.mapper.CatImgMapper;
import com.xs.cathome.domain.CatImg;
import com.xs.cathome.service.ICatImgService;

/**
 * 文章图片 评论图片Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-14
 */
@Service
public class CatImgServiceImpl implements ICatImgService 
{
    @Autowired
    private CatImgMapper catImgMapper;

    /**
     * 查询文章图片 评论图片
     * 
     * @param imgId 文章图片 评论图片主键
     * @return 文章图片 评论图片
     */
    @Override
    public CatImg selectCatImgByImgId(String imgId)
    {
        return catImgMapper.selectCatImgByImgId(imgId);
    }

    /**
     * 查询文章图片 评论图片列表
     * 
     * @param catImg 文章图片 评论图片
     * @return 文章图片 评论图片
     */
    @Override
    public List<CatImg> selectCatImgList(CatImg catImg)
    {
        return catImgMapper.selectCatImgList(catImg);
    }

    /**
     * 新增文章图片 评论图片
     * 
     * @param catImg 文章图片 评论图片
     * @return 结果
     */
    @Override
    public int insertCatImg(CatImg catImg)
    {
        return catImgMapper.insertCatImg(catImg);
    }

    /**
     * 修改文章图片 评论图片
     * 
     * @param catImg 文章图片 评论图片
     * @return 结果
     */
    @Override
    public int updateCatImg(CatImg catImg)
    {
        return catImgMapper.updateCatImg(catImg);
    }

    /**
     * 批量删除文章图片 评论图片
     * 
     * @param imgIds 需要删除的文章图片 评论图片主键
     * @return 结果
     */
    @Override
    public int deleteCatImgByImgIds(String[] imgIds)
    {
        return catImgMapper.deleteCatImgByImgIds(imgIds);
    }

    /**
     * 删除文章图片 评论图片信息
     * 
     * @param imgId 文章图片 评论图片主键
     * @return 结果
     */
    @Override
    public int deleteCatImgByImgId(String imgId)
    {
        return catImgMapper.deleteCatImgByImgId(imgId);
    }
}
