package com.xs.cathome.service;

import com.xs.cathome.domain.CatUserConcern;

import java.util.List;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2023-03-13
 */
public interface ICatUserConcernService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public CatUserConcern selectCatUserConcernById(String id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param catUserConcern 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<CatUserConcern> selectCatUserConcernList(CatUserConcern catUserConcern);

    /**
     * 新增【请填写功能名称】
     * 
     * @param catUserConcern 【请填写功能名称】
     * @return 结果
     */
    public int insertCatUserConcern(CatUserConcern catUserConcern);

    /**
     * 修改【请填写功能名称】
     * 
     * @param catUserConcern 【请填写功能名称】
     * @return 结果
     */
    public int updateCatUserConcern(CatUserConcern catUserConcern);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteCatUserConcernByIds(Long[] ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteCatUserConcernById(Long id);
}
