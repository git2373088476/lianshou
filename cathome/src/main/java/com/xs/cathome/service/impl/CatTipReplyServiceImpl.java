package com.xs.cathome.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.ruoyi.common.utils.DateUtils;
import com.xs.cathome.domain.CatUser;
import com.xs.cathome.domain.CatUserConcern;
import com.xs.cathome.domain.vo.CatUserAndTipRepliesVO;
import com.xs.cathome.service.ICatUserConcernService;
import com.xs.cathome.service.ICatUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xs.cathome.mapper.CatTipReplyMapper;
import com.xs.cathome.domain.CatTipReply;
import com.xs.cathome.service.ICatTipReplyService;

import javax.lang.model.element.VariableElement;

/**
 * 打赏评论Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-14
 */
@Service
public class CatTipReplyServiceImpl implements ICatTipReplyService 
{
    @Autowired
    private CatTipReplyMapper catTipReplyMapper;

    @Autowired
    private ICatUserService catUserService;

    /**
     * 查询打赏评论
     * 
     * @param userId 打赏评论主键
     * @return 打赏评论
     */
    @Override
    public CatTipReply selectCatTipReplyByUserId(String userId)
    {
        return catTipReplyMapper.selectCatTipReplyByUserId(userId);
    }

    /**
     * 查询打赏评论列表
     * 
     * @param catTipReply 打赏评论
     * @return 打赏评论
     */
    @Override
    public List<CatTipReply> selectCatTipReplyList(CatTipReply catTipReply)
    {
        return catTipReplyMapper.selectCatTipReplyList(catTipReply);
    }

    /**
     * 新增打赏评论
     * 
     * @param catTipReply 打赏评论
     * @return 结果
     */
    @Override
    public int insertCatTipReply(CatTipReply catTipReply)
    {
        catTipReply.setCreateTime(DateUtils.getNowDate());
        return catTipReplyMapper.insertCatTipReply(catTipReply);
    }

    /**
     * 修改打赏评论
     * 
     * @param catTipReply 打赏评论
     * @return 结果
     */
    @Override
    public int updateCatTipReply(CatTipReply catTipReply)
    {
        catTipReply.setUpdateTime(DateUtils.getNowDate());
        return catTipReplyMapper.updateCatTipReply(catTipReply);
    }

    /**
     * 批量删除打赏评论
     * 
     * @param userIds 需要删除的打赏评论主键
     * @return 结果
     */
    @Override
    public int deleteCatTipReplyByUserIds(String[] userIds)
    {
        return catTipReplyMapper.deleteCatTipReplyByUserIds(userIds);
    }

    /**
     * 删除打赏评论信息
     * 
     * @param userId 打赏评论主键
     * @return 结果
     */
    @Override
    public int deleteCatTipReplyByUserId(String userId)
    {
        return catTipReplyMapper.deleteCatTipReplyByUserId(userId);
    }

    @Override
    public List<CatUserAndTipRepliesVO> selectTipReplyList(String articleId) {
        List<CatTipReply> catTipReplies = catTipReplyMapper.selectTipReplyList(articleId);
        List<CatUserAndTipRepliesVO> catUserAndTipRepliesVOS = catTipReplies.stream().map((item) -> {
            CatUser catUser = catUserService.selectCatUserByUserId(item.getUserId());
            CatUserAndTipRepliesVO catUserAndTipRepliesVO = new CatUserAndTipRepliesVO();
            catUserAndTipRepliesVO.setUserId(catUser.getUserId());
            catUserAndTipRepliesVO.setUserName(catUser.getUserName());
            catUserAndTipRepliesVO.setUserLogo(catUser.getUserLogo());
            catUserAndTipRepliesVO.setArticleId(item.getArticleId());
            catUserAndTipRepliesVO.setArticle(item.getArticle());
            catUserAndTipRepliesVO.setNumLike(item.getNumLike());
            catUserAndTipRepliesVO.setArticlePid(item.getArticlePid());
            return catUserAndTipRepliesVO;
        }).collect(Collectors.toList());
        return catUserAndTipRepliesVOS;
    }
}
