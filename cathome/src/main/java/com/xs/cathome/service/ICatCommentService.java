package com.xs.cathome.service;

import java.util.List;
import com.xs.cathome.domain.CatComment;

/**
 * 评论Service接口
 * 
 * @author ruoyi
 * @date 2023-03-14
 */
public interface ICatCommentService 
{
    /**
     * 查询评论
     * 
     * @param commentId 评论主键
     * @return 评论
     */
    public CatComment selectCatCommentByCommentId(String commentId);

    /**
     * 查询评论列表
     * 
     * @param catComment 评论
     * @return 评论集合
     */
    public List<CatComment> selectCatCommentList(CatComment catComment);

    /**
     * 新增评论
     * 
     * @param catComment 评论
     * @return 结果
     */
    public int insertCatComment(CatComment catComment);

    /**
     * 修改评论
     * 
     * @param catComment 评论
     * @return 结果
     */
    public int updateCatComment(CatComment catComment);

    /**
     * 批量删除评论
     * 
     * @param commentIds 需要删除的评论主键集合
     * @return 结果
     */
    public int deleteCatCommentByCommentIds(String[] commentIds);

    /**
     * 删除评论信息
     * 
     * @param commentId 评论主键
     * @return 结果
     */
    public int deleteCatCommentByCommentId(String commentId);
}
