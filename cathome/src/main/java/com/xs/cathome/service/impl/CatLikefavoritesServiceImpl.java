package com.xs.cathome.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xs.cathome.mapper.CatLikefavoritesMapper;
import com.xs.cathome.domain.CatLikefavorites;
import com.xs.cathome.service.ICatLikefavoritesService;

/**
 * 点赞收藏Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-14
 */
@Service
public class CatLikefavoritesServiceImpl implements ICatLikefavoritesService 
{
    @Autowired
    private CatLikefavoritesMapper catLikefavoritesMapper;

    /**
     * 查询点赞收藏
     * 
     * @param id 点赞收藏主键
     * @return 点赞收藏
     */
    @Override
    public CatLikefavorites selectCatLikefavoritesById(Long id)
    {
        return catLikefavoritesMapper.selectCatLikefavoritesById(id);
    }

    /**
     * 查询点赞收藏列表
     * 
     * @param catLikefavorites 点赞收藏
     * @return 点赞收藏
     */
    @Override
    public List<CatLikefavorites> selectCatLikefavoritesList(CatLikefavorites catLikefavorites)
    {
        return catLikefavoritesMapper.selectCatLikefavoritesList(catLikefavorites);
    }

    /**
     * 新增点赞收藏
     * 
     * @param catLikefavorites 点赞收藏
     * @return 结果
     */
    @Override
    public int insertCatLikefavorites(CatLikefavorites catLikefavorites)
    {
        return catLikefavoritesMapper.insertCatLikefavorites(catLikefavorites);
    }

    /**
     * 修改点赞收藏
     * 
     * @param catLikefavorites 点赞收藏
     * @return 结果
     */
    @Override
    public int updateCatLikefavorites(CatLikefavorites catLikefavorites)
    {
        return catLikefavoritesMapper.updateCatLikefavorites(catLikefavorites);
    }

    /**
     * 批量删除点赞收藏
     * 
     * @param ids 需要删除的点赞收藏主键
     * @return 结果
     */
    @Override
    public int deleteCatLikefavoritesByIds(Long[] ids)
    {
        return catLikefavoritesMapper.deleteCatLikefavoritesByIds(ids);
    }

    /**
     * 删除点赞收藏信息
     * 
     * @param id 点赞收藏主键
     * @return 结果
     */
    @Override
    public int deleteCatLikefavoritesById(Long id)
    {
        return catLikefavoritesMapper.deleteCatLikefavoritesById(id);
    }
}
