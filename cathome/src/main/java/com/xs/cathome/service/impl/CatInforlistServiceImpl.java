package com.xs.cathome.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.xs.cathome.domain.CatTipArticle;
import com.xs.cathome.domain.vo.CatinfoVO;
import com.xs.cathome.service.ICatTipArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xs.cathome.mapper.CatInforlistMapper;
import com.xs.cathome.domain.CatInforlist;
import com.xs.cathome.service.ICatInforlistService;

/**
 * 资讯关系表Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-15
 */
@Service
public class CatInforlistServiceImpl implements ICatInforlistService 
{
    @Autowired
    private CatInforlistMapper catInforlistMapper;

    @Autowired
    private ICatTipArticleService catTipArticleService;

    /**
     * 查询资讯关系表
     * 
     * @param inforlistId 资讯关系表主键
     * @return 资讯关系表
     */
    @Override
    public CatInforlist selectCatInforlistByInforlistId(String inforlistId)
    {
        return catInforlistMapper.selectCatInforlistByInforlistId(inforlistId);
    }

    /**
     * 查询资讯关系表列表
     * 
     * @param catInforlist 资讯关系表
     * @return 资讯关系表
     */
    @Override
    public List<CatInforlist> selectCatInforlistList(CatInforlist catInforlist)
    {
        return catInforlistMapper.selectCatInforlistList(catInforlist);
    }

    /**
     * 新增资讯关系表
     * 
     * @param catInforlist 资讯关系表
     * @return 结果
     */
    @Override
    public int insertCatInforlist(CatInforlist catInforlist)
    {
        catInforlist.setCreateTime(DateUtils.getNowDate());
        return catInforlistMapper.insertCatInforlist(catInforlist);
    }

    /**
     * 修改资讯关系表
     * 
     * @param catInforlist 资讯关系表
     * @return 结果
     */
    @Override
    public int updateCatInforlist(CatInforlist catInforlist)
    {
        catInforlist.setUpdateTime(DateUtils.getNowDate());
        return catInforlistMapper.updateCatInforlist(catInforlist);
    }

    /**
     * 批量删除资讯关系表
     * 
     * @param inforlistIds 需要删除的资讯关系表主键
     * @return 结果
     */
    @Override
    public int deleteCatInforlistByInforlistIds(String[] inforlistIds)
    {
        return catInforlistMapper.deleteCatInforlistByInforlistIds(inforlistIds);
    }

    /**
     * 删除资讯关系表信息
     * 
     * @param inforlistId 资讯关系表主键
     * @return 结果
     */
    @Override
    public int deleteCatInforlistByInforlistId(String inforlistId)
    {
        return catInforlistMapper.deleteCatInforlistByInforlistId(inforlistId);
    }

    @Override
    public int insertCatInfor(CatinfoVO catinfoVO) {
        CatTipArticle catTipArticle = catinfoVO.getCatTipArticle();
        String articleReturnId = catTipArticleService.insertCatTipArticleReturnID(catTipArticle);
        CatInforlist catInforlist = new CatInforlist();
        catInforlist.setInforlistId(IdUtils.fastSimpleUUID());
        catInforlist.setInforTypeId(catinfoVO.getInforTypeId());
        catInforlist.setArticleId(articleReturnId);
        return catInforlistMapper.insertCatInforlist(catInforlist);
    }

    @Override
    public List<CatinfoVO> getInfoByType(String inforTypeId) {
        List<CatInforlist> catInforlist = catInforlistMapper.selectCatInforlistByInforTypeId(inforTypeId);
        List<CatinfoVO> collect = catInforlist.stream().map((item) -> {
            CatinfoVO catinfoVO = new CatinfoVO();
            CatTipArticle catTipArticle = catTipArticleService.selectCatTipArticleByArticleId(item.getArticleId());
            catinfoVO.setInforTypeId(inforTypeId);
            catinfoVO.setCatTipArticle(catTipArticle);
            return catinfoVO;
        }).collect(Collectors.toList());
        return collect;
    }
}
