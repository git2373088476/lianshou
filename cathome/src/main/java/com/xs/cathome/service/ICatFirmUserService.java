package com.xs.cathome.service;

import java.util.List;
import com.xs.cathome.domain.CatFirmUser;

/**
 * 公司员工表Service接口
 * 
 * @author ruoyi
 * @date 2023-03-15
 */
public interface ICatFirmUserService 
{
    /**
     * 查询公司员工表
     * 
     * @param id 公司员工表主键
     * @return 公司员工表
     */
    public CatFirmUser selectCatFirmUserById(String id);

    /**
     * 查询公司员工表列表
     * 
     * @param catFirmUser 公司员工表
     * @return 公司员工表集合
     */
    public List<CatFirmUser> selectCatFirmUserList(CatFirmUser catFirmUser);

    /**
     * 新增公司员工表
     * 
     * @param catFirmUser 公司员工表
     * @return 结果
     */
    public int insertCatFirmUser(CatFirmUser catFirmUser);

    /**
     * 修改公司员工表
     * 
     * @param catFirmUser 公司员工表
     * @return 结果
     */
    public int updateCatFirmUser(CatFirmUser catFirmUser);

    /**
     * 批量删除公司员工表
     * 
     * @param ids 需要删除的公司员工表主键集合
     * @return 结果
     */
    public int deleteCatFirmUserByIds(String[] ids);

    /**
     * 删除公司员工表信息
     * 
     * @param id 公司员工表主键
     * @return 结果
     */
    public int deleteCatFirmUserById(String id);
}
