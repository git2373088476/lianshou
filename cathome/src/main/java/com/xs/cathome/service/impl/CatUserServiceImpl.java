package com.xs.cathome.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.xs.cathome.domain.CatUser;
import com.xs.cathome.mapper.CatUserMapper;
import com.xs.cathome.service.ICatUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-13
 */
@Service
public class CatUserServiceImpl implements ICatUserService
{
    @Autowired
    private CatUserMapper catUserMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param userId 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public CatUser selectCatUserByUserId(String userId)
    {
        return catUserMapper.selectCatUserByUserId(userId);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param catUser 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<CatUser> selectCatUserList(CatUser catUser)
    {
        return catUserMapper.selectCatUserList(catUser);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param catUser 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertCatUser(CatUser catUser)
    {
        catUser.setCreateTime(DateUtils.getNowDate());
        return catUserMapper.insertCatUser(catUser);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param catUser 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateCatUser(CatUser catUser)
    {
        catUser.setUpdateTime(DateUtils.getNowDate());
        return catUserMapper.updateCatUser(catUser);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param userIds 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteCatUserByUserIds(Long[] userIds)
    {
        return catUserMapper.deleteCatUserByUserIds(userIds);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param userId 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteCatUserByUserId(Long userId)
    {
        return catUserMapper.deleteCatUserByUserId(userId);
    }
}
