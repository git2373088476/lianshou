package com.xs.cathome.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xs.cathome.mapper.CatTipCollectionMapper;
import com.xs.cathome.domain.CatTipCollection;
import com.xs.cathome.service.ICatTipCollectionService;

/**
 * 打赏收藏Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-14
 */
@Service
public class CatTipCollectionServiceImpl implements ICatTipCollectionService 
{
    @Autowired
    private CatTipCollectionMapper catTipCollectionMapper;

    /**
     * 查询打赏收藏
     * 
     * @param collectionId 打赏收藏主键
     * @return 打赏收藏
     */
    @Override
    public CatTipCollection selectCatTipCollectionByCollectionId(String collectionId)
    {
        return catTipCollectionMapper.selectCatTipCollectionByCollectionId(collectionId);
    }

    /**
     * 查询打赏收藏列表
     * 
     * @param catTipCollection 打赏收藏
     * @return 打赏收藏
     */
    @Override
    public List<CatTipCollection> selectCatTipCollectionList(CatTipCollection catTipCollection)
    {
        return catTipCollectionMapper.selectCatTipCollectionList(catTipCollection);
    }

    /**
     * 新增打赏收藏
     * 
     * @param catTipCollection 打赏收藏
     * @return 结果
     */
    @Override
    public int insertCatTipCollection(CatTipCollection catTipCollection)
    {
        catTipCollection.setCreateTime(DateUtils.getNowDate());
        return catTipCollectionMapper.insertCatTipCollection(catTipCollection);
    }

    /**
     * 修改打赏收藏
     * 
     * @param catTipCollection 打赏收藏
     * @return 结果
     */
    @Override
    public int updateCatTipCollection(CatTipCollection catTipCollection)
    {
        catTipCollection.setUpdateTime(DateUtils.getNowDate());
        return catTipCollectionMapper.updateCatTipCollection(catTipCollection);
    }

    /**
     * 批量删除打赏收藏
     * 
     * @param collectionIds 需要删除的打赏收藏主键
     * @return 结果
     */
    @Override
    public int deleteCatTipCollectionByCollectionIds(String[] collectionIds)
    {
        return catTipCollectionMapper.deleteCatTipCollectionByCollectionIds(collectionIds);
    }

    /**
     * 删除打赏收藏信息
     * 
     * @param collectionId 打赏收藏主键
     * @return 结果
     */
    @Override
    public int deleteCatTipCollectionByCollectionId(String collectionId)
    {
        return catTipCollectionMapper.deleteCatTipCollectionByCollectionId(collectionId);
    }
}
