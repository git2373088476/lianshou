package com.xs.cathome.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.xs.cathome.domain.CatFirmHonorArticle;
import com.xs.cathome.domain.CatTipArticle;
import com.xs.cathome.domain.CatUser;
import com.xs.cathome.domain.vo.CatFirmUserVO;
import com.xs.cathome.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xs.cathome.mapper.CatFirmHonorMapper;
import com.xs.cathome.domain.CatFirmHonor;
import org.springframework.transaction.annotation.Transactional;

/**
 * 公司荣誉表Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-15
 */
@Service
public class CatFirmHonorServiceImpl implements ICatFirmHonorService 
{
    @Autowired
    private CatFirmHonorMapper catFirmHonorMapper;

    @Autowired
    private ICatTipArticleService catTipArticleService;

    @Autowired
    private ICatFirmHonorArticleService catFirmHonorArticleService;

    @Autowired
    private ICatUserService catUserService;

    @Autowired
    private ICatFirmService catFirmService;


    /**
     * 查询公司荣誉表
     * 
     * @param honorId 公司荣誉表主键
     * @return 公司荣誉表
     */
    @Override
    public CatFirmHonor selectCatFirmHonorByHonorId(String honorId)
    {
        return catFirmHonorMapper.selectCatFirmHonorByHonorId(honorId);
    }

    /**
     * 查询公司荣誉表列表
     * 
     * @param catFirmHonor 公司荣誉表
     * @return 公司荣誉表
     */
    @Override
    public List<CatFirmHonor> selectCatFirmHonorList(CatFirmHonor catFirmHonor)
    {
        return catFirmHonorMapper.selectCatFirmHonorList(catFirmHonor);
    }

    /**
     * 新增公司荣誉表
     * 
     * @param catFirmHonor 公司荣誉表
     * @return 结果
     */
    @Override
    public int insertCatFirmHonor(CatFirmHonor catFirmHonor)
    {
        catFirmHonor.setCreateTime(DateUtils.getNowDate());
        return catFirmHonorMapper.insertCatFirmHonor(catFirmHonor);
    }

    /**
     * 修改公司荣誉表
     * 
     * @param catFirmHonor 公司荣誉表
     * @return 结果
     */
    @Override
    public int updateCatFirmHonor(CatFirmHonor catFirmHonor)
    {
        catFirmHonor.setUpdateTime(DateUtils.getNowDate());
        return catFirmHonorMapper.updateCatFirmHonor(catFirmHonor);
    }

    /**
     * 批量删除公司荣誉表
     * 
     * @param honorIds 需要删除的公司荣誉表主键
     * @return 结果
     */
    @Override
    public int deleteCatFirmHonorByHonorIds(String[] honorIds)
    {
        return catFirmHonorMapper.deleteCatFirmHonorByHonorIds(honorIds);
    }

    /**
     * 删除公司荣誉表信息
     * 
     * @param honorId 公司荣誉表主键
     * @return 结果
     */
    @Override
    public int deleteCatFirmHonorByHonorId(String honorId)
    {
        return catFirmHonorMapper.deleteCatFirmHonorByHonorId(honorId);
    }

    /**
     * 新增公司荣誉
     * @param catFirmUserVO
     * @return
     */
    @Transactional
    @Override
    public int addhonor(CatFirmUserVO catFirmUserVO) {
//        新增公司荣誉表
        CatFirmHonor catFirmHonor = new CatFirmHonor();
        catFirmHonor.setHonorId(IdUtils.simpleUUID());
        catFirmHonor.setFirmId(catFirmUserVO.getFirmId());
        catFirmHonor.setUserId(catFirmUserVO.getUserId());
        int firmHonor = catFirmHonorMapper.insertCatFirmHonor(catFirmHonor);
//        新增荣誉
        CatTipArticle catTipArticle = catFirmUserVO.getCatTipArticle();
        catTipArticle.setArticleId(IdUtils.simpleUUID());
        int tipArticle = catTipArticleService.insertCatTipArticle(catTipArticle);
//        新增荣誉、评论表
        CatFirmHonorArticle catFirmHonorArticle = new CatFirmHonorArticle();
        catFirmHonorArticle.setArtHonorId(IdUtils.simpleUUID());
        catFirmHonorArticle.setHonorId(catFirmHonor.getHonorId());
        catFirmHonorArticle.setArticleId(catTipArticle.getArticleId());
        return catFirmHonorArticleService.insertCatFirmHonorArticle(catFirmHonorArticle);
    }

    /**
     * 查询公司荣誉表列表
     * @param firmId
     * @return
     */
    @Transactional
    @Override
    public List<CatFirmUserVO> listhonor(String firmId) {
//        根据公司id查找公司名字
        String firmName = catFirmService.selectCatFirmByFirmId(firmId).getFirmName();
//        查询出公司荣誉评论表
        List<CatFirmHonor> catFirmHonorList = catFirmHonorMapper.selectCatFirmHonorByFirmId(firmId);
        List<CatFirmUserVO> collect = catFirmHonorList.stream().map((item) -> {
            String userId = item.getUserId();
//            根据公司荣誉id 查询评论id
            CatFirmHonorArticle catFirmHonorArticle = catFirmHonorArticleService.selectCatFirmHonorArticleByHonorId(item.getHonorId());
            String articleId = catFirmHonorArticle.getArticleId();
//            根据评论id和用户id 查询出某公司员工发布的文章
            CatTipArticle catTipArticleList = catTipArticleService.selectCatTipArticleByArticleIdAndUserId(articleId, userId);
//            根据返回的发布文章catTipArticleList 查找出用户的信息
            CatUser catUser = catUserService.selectCatUserByUserId(catTipArticleList.getUserId());
//            组装数据 返回CatFirmUserVO
            CatFirmUserVO catFirmUserVO = new CatFirmUserVO();
            catFirmUserVO.setHonorId(item.getHonorId());
            catFirmUserVO.setFirmId(firmId);
            catFirmUserVO.setFirmName(firmName);
            catFirmUserVO.setUserId(userId);
            catFirmUserVO.setUserName(catUser.getUserName());
            catFirmUserVO.setUserLogo(catUser.getUserLogo());
            catFirmUserVO.setCatTipArticle(catTipArticleList);
            return catFirmUserVO;
        }).collect(Collectors.toList());
        return collect;
    }
}
