package com.xs.cathome.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xs.cathome.mapper.CatFirmHonorArticleMapper;
import com.xs.cathome.domain.CatFirmHonorArticle;
import com.xs.cathome.service.ICatFirmHonorArticleService;

/**
 * 公司荣誉和评论Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-15
 */
@Service
public class CatFirmHonorArticleServiceImpl implements ICatFirmHonorArticleService 
{
    @Autowired
    private CatFirmHonorArticleMapper catFirmHonorArticleMapper;

    /**
     * 查询公司荣誉和评论
     * 
     * @param artHonorId 公司荣誉和评论主键
     * @return 公司荣誉和评论
     */
    @Override
    public CatFirmHonorArticle selectCatFirmHonorArticleByArtHonorId(String artHonorId)
    {
        return catFirmHonorArticleMapper.selectCatFirmHonorArticleByArtHonorId(artHonorId);
    }

    /**
     * 查询公司荣誉和评论列表
     * 
     * @param catFirmHonorArticle 公司荣誉和评论
     * @return 公司荣誉和评论
     */
    @Override
    public List<CatFirmHonorArticle> selectCatFirmHonorArticleList(CatFirmHonorArticle catFirmHonorArticle)
    {
        return catFirmHonorArticleMapper.selectCatFirmHonorArticleList(catFirmHonorArticle);
    }

    /**
     * 新增公司荣誉和评论
     * 
     * @param catFirmHonorArticle 公司荣誉和评论
     * @return 结果
     */
    @Override
    public int insertCatFirmHonorArticle(CatFirmHonorArticle catFirmHonorArticle)
    {
        catFirmHonorArticle.setCreateTime(DateUtils.getNowDate());
        return catFirmHonorArticleMapper.insertCatFirmHonorArticle(catFirmHonorArticle);
    }

    /**
     * 修改公司荣誉和评论
     * 
     * @param catFirmHonorArticle 公司荣誉和评论
     * @return 结果
     */
    @Override
    public int updateCatFirmHonorArticle(CatFirmHonorArticle catFirmHonorArticle)
    {
        catFirmHonorArticle.setUpdateTime(DateUtils.getNowDate());
        return catFirmHonorArticleMapper.updateCatFirmHonorArticle(catFirmHonorArticle);
    }

    /**
     * 批量删除公司荣誉和评论
     * 
     * @param artHonorIds 需要删除的公司荣誉和评论主键
     * @return 结果
     */
    @Override
    public int deleteCatFirmHonorArticleByArtHonorIds(String[] artHonorIds)
    {
        return catFirmHonorArticleMapper.deleteCatFirmHonorArticleByArtHonorIds(artHonorIds);
    }

    /**
     * 删除公司荣誉和评论信息
     * 
     * @param artHonorId 公司荣誉和评论主键
     * @return 结果
     */
    @Override
    public int deleteCatFirmHonorArticleByArtHonorId(String artHonorId)
    {
        return catFirmHonorArticleMapper.deleteCatFirmHonorArticleByArtHonorId(artHonorId);
    }

    @Override
    public CatFirmHonorArticle selectCatFirmHonorArticleByHonorId(String honorId) {

        return catFirmHonorArticleMapper.selectCatFirmHonorArticleByHonorId(honorId);
    }
}
