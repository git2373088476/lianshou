package com.xs.cathome.service;

import java.util.List;
import com.xs.cathome.domain.CatFleaArticleMarket;

/**
 * 跳蚤信息关联表Service接口
 * 
 * @author ruoyi
 * @date 2023-03-16
 */
public interface ICatFleaArticleMarketService 
{
    /**
     * 查询跳蚤信息关联表
     * 
     * @param artMarId 跳蚤信息关联表主键
     * @return 跳蚤信息关联表
     */
    public CatFleaArticleMarket selectCatFleaArticleMarketByArtMarId(String artMarId);

    /**
     * 查询跳蚤信息关联表列表
     * 
     * @param catFleaArticleMarket 跳蚤信息关联表
     * @return 跳蚤信息关联表集合
     */
    public List<CatFleaArticleMarket> selectCatFleaArticleMarketList(CatFleaArticleMarket catFleaArticleMarket);

    /**
     * 新增跳蚤信息关联表
     * 
     * @param catFleaArticleMarket 跳蚤信息关联表
     * @return 结果
     */
    public int insertCatFleaArticleMarket(CatFleaArticleMarket catFleaArticleMarket);

    /**
     * 修改跳蚤信息关联表
     * 
     * @param catFleaArticleMarket 跳蚤信息关联表
     * @return 结果
     */
    public int updateCatFleaArticleMarket(CatFleaArticleMarket catFleaArticleMarket);

    /**
     * 批量删除跳蚤信息关联表
     * 
     * @param artMarIds 需要删除的跳蚤信息关联表主键集合
     * @return 结果
     */
    public int deleteCatFleaArticleMarketByArtMarIds(String[] artMarIds);

    /**
     * 删除跳蚤信息关联表信息
     * 
     * @param artMarId 跳蚤信息关联表主键
     * @return 结果
     */
    public int deleteCatFleaArticleMarketByArtMarId(String artMarId);
}
