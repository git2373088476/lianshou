package com.xs.cathome.service.impl;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.ruoyi.common.utils.uuid.IdUtils;
import com.xs.cathome.domain.CatFleaArticleMarket;
import com.xs.cathome.domain.CatTipArticle;
import com.xs.cathome.domain.vo.CatFleaMarketVO;
import com.xs.cathome.service.ICatFleaArticleMarketService;
import com.xs.cathome.service.ICatTipArticleService;
import jdk.nashorn.internal.runtime.RewriteException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xs.cathome.mapper.CatFleaMarketMapper;
import com.xs.cathome.domain.CatFleaMarket;
import com.xs.cathome.service.ICatFleaMarketService;
import org.springframework.transaction.annotation.Transactional;

import javax.swing.plaf.ProgressBarUI;

/**
 * 跳蚤市场Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-16
 */
@Service
public class CatFleaMarketServiceImpl implements ICatFleaMarketService 
{
    @Autowired
    private CatFleaMarketMapper catFleaMarketMapper;

    @Autowired
    private ICatTipArticleService catTipArticleService;

    @Autowired
    private ICatFleaArticleMarketService catFleaArticleMarketService;

    /**
     * 查询跳蚤市场
     * 
     * @param marketId 跳蚤市场主键
     * @return 跳蚤市场
     */
    @Override
    public CatFleaMarket selectCatFleaMarketByMarketId(String marketId)
    {
        return catFleaMarketMapper.selectCatFleaMarketByMarketId(marketId);
    }

    /**
     * 查询跳蚤市场列表
     * 
     * @param catFleaMarket 跳蚤市场
     * @return 跳蚤市场
     */
    @Override
    public List<CatFleaMarket> selectCatFleaMarketList(CatFleaMarket catFleaMarket)
    {
        return catFleaMarketMapper.selectCatFleaMarketList(catFleaMarket);
    }

    /**
     * 新增跳蚤市场
     * 
     * @param catFleaMarket 跳蚤市场
     * @return 结果
     */
    @Override
    public int insertCatFleaMarket(CatFleaMarket catFleaMarket)
    {
        return catFleaMarketMapper.insertCatFleaMarket(catFleaMarket);
    }

    /**
     * 修改跳蚤市场
     * 
     * @param catFleaMarket 跳蚤市场
     * @return 结果
     */
    @Override
    public int updateCatFleaMarket(CatFleaMarket catFleaMarket)
    {
        return catFleaMarketMapper.updateCatFleaMarket(catFleaMarket);
    }

    /**
     * 批量删除跳蚤市场
     * 
     * @param marketIds 需要删除的跳蚤市场主键
     * @return 结果
     */
    @Override
    public int deleteCatFleaMarketByMarketIds(String[] marketIds)
    {
        return catFleaMarketMapper.deleteCatFleaMarketByMarketIds(marketIds);
    }

    /**
     * 删除跳蚤市场信息
     * 
     * @param marketId 跳蚤市场主键
     * @return 结果
     */
    @Override
    public int deleteCatFleaMarketByMarketId(String marketId)
    {
        return catFleaMarketMapper.deleteCatFleaMarketByMarketId(marketId);
    }


    @Transactional
    @Override
    public int addmarket(CatFleaMarketVO catFleaMarketVO) {
        //跳蚤表插入
        CatFleaMarket catFleaMarket = catFleaMarketVO.getCatFleaMarket();
        catFleaMarket.setMarketId(IdUtils.simpleUUID());
        catFleaMarketMapper.insertCatFleaMarket(catFleaMarket);
        //信息表插入
        CatTipArticle catTipArticle = catFleaMarketVO.getCatTipArticle();
        catTipArticle.setArticleId(IdUtils.simpleUUID());
        catTipArticleService.insertCatTipArticleReturnID(catTipArticle);
        //跳蚤和信息关联表
        CatFleaArticleMarket catFleaArticleMarket = new CatFleaArticleMarket();
        catFleaArticleMarket.setArtMarId(IdUtils.simpleUUID());
        catFleaArticleMarket.setMarketId(catFleaMarket.getMarketId());
        catFleaArticleMarket.setArticleId(catTipArticle.getArticleId());
        int result = catFleaArticleMarketService.insertCatFleaArticleMarket(catFleaArticleMarket);
        return result;
    }

    @Override
    public List<CatFleaMarketVO> selectMarketList(CatFleaMarketVO catFleaMarketVO) {
        if (catFleaMarketVO != null){
            CatFleaMarket catFleaMarket = catFleaMarketVO.getCatFleaMarket();
            CatTipArticle catTipArticle = catFleaMarketVO.getCatTipArticle();
            if (catFleaMarket != null && catTipArticle !=null){
                //查询出推荐、、、等
                List<CatTipArticle> catTipArticleList = catTipArticleService.selectCatTipArticleList(catFleaMarket.getType());
                //查询类型
                List<CatFleaMarket> catFleaMarkets = catFleaMarketMapper.selectCatFleaMarketList(catFleaMarket);
                List<CatFleaMarketVO> list = catFleaMarkets.stream().map((item) -> {
                    CatFleaArticleMarket catFleaArticleMarket = new CatFleaArticleMarket();
                    catFleaArticleMarket.setMarketId(item.getMarketId());
                    List<CatFleaArticleMarket> catFleaArticleMarkets = catFleaArticleMarketService.selectCatFleaArticleMarketList(catFleaArticleMarket);
                    CatFleaArticleMarket fleaArticleMarket = catFleaArticleMarkets.get(0);
                    List<CatFleaMarketVO> collect = catTipArticleList.stream().map((ctal) -> {
                        if (ctal.getArticleId().equals(fleaArticleMarket.getArticleId())) {
                            CatFleaMarketVO fleaMarketVO = new CatFleaMarketVO();
                            fleaMarketVO.setCatFleaMarket(item);
                            fleaMarketVO.setCatTipArticle(ctal);
                            return fleaMarketVO;
                        }
                        return null;
                    }).collect(Collectors.toList());
                    collect.removeAll(Collections.singleton(null));
                    CatFleaMarketVO vo = collect.get(0);
                    return vo;
                }).collect(Collectors.toList());
                return list;
            }else {
                return null;
            }
        }

        return null;
    }
}
