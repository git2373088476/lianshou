package com.xs.cathome.service;

import java.util.List;
import com.xs.cathome.domain.CatFirm;

/**
 * 公司表Service接口
 * 
 * @author ruoyi
 * @date 2023-03-15
 */
public interface ICatFirmService 
{
    /**
     * 查询公司表
     * 
     * @param firmId 公司表主键
     * @return 公司表
     */
    public CatFirm selectCatFirmByFirmId(String firmId);

    /**
     * 查询公司表列表
     * 
     * @param catFirm 公司表
     * @return 公司表集合
     */
    public List<CatFirm> selectCatFirmList(CatFirm catFirm);

    /**
     * 新增公司表
     * 
     * @param catFirm 公司表
     * @return 结果
     */
    public int insertCatFirm(CatFirm catFirm);

    /**
     * 修改公司表
     * 
     * @param catFirm 公司表
     * @return 结果
     */
    public int updateCatFirm(CatFirm catFirm);

    /**
     * 批量删除公司表
     * 
     * @param firmIds 需要删除的公司表主键集合
     * @return 结果
     */
    public int deleteCatFirmByFirmIds(String[] firmIds);

    /**
     * 删除公司表信息
     * 
     * @param firmId 公司表主键
     * @return 结果
     */
    public int deleteCatFirmByFirmId(String firmId);
}
