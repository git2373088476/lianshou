package com.xs.cathome.service;

import java.util.List;
import com.xs.cathome.domain.CatFirmHonor;
import com.xs.cathome.domain.vo.CatFirmUserVO;

/**
 * 公司荣誉表Service接口
 * 
 * @author ruoyi
 * @date 2023-03-15
 */
public interface ICatFirmHonorService 
{
    /**
     * 查询公司荣誉表
     * 
     * @param honorId 公司荣誉表主键
     * @return 公司荣誉表
     */
    public CatFirmHonor selectCatFirmHonorByHonorId(String honorId);

    /**
     * 查询公司荣誉表列表
     * 
     * @param catFirmHonor 公司荣誉表
     * @return 公司荣誉表集合
     */
    public List<CatFirmHonor> selectCatFirmHonorList(CatFirmHonor catFirmHonor);

    /**
     * 新增公司荣誉表
     * 
     * @param catFirmHonor 公司荣誉表
     * @return 结果
     */
    public int insertCatFirmHonor(CatFirmHonor catFirmHonor);

    /**
     * 修改公司荣誉表
     * 
     * @param catFirmHonor 公司荣誉表
     * @return 结果
     */
    public int updateCatFirmHonor(CatFirmHonor catFirmHonor);

    /**
     * 批量删除公司荣誉表
     * 
     * @param honorIds 需要删除的公司荣誉表主键集合
     * @return 结果
     */
    public int deleteCatFirmHonorByHonorIds(String[] honorIds);

    /**
     * 删除公司荣誉表信息
     * 
     * @param honorId 公司荣誉表主键
     * @return 结果
     */
    public int deleteCatFirmHonorByHonorId(String honorId);

    int addhonor(CatFirmUserVO catFirmUserVO);

    List<CatFirmUserVO> listhonor(String firmId);
}
