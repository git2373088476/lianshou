package com.xs.cathome.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xs.cathome.mapper.CatFirmMapper;
import com.xs.cathome.domain.CatFirm;
import com.xs.cathome.service.ICatFirmService;

/**
 * 公司表Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-15
 */
@Service
public class CatFirmServiceImpl implements ICatFirmService 
{
    @Autowired
    private CatFirmMapper catFirmMapper;

    /**
     * 查询公司表
     * 
     * @param firmId 公司表主键
     * @return 公司表
     */
    @Override
    public CatFirm selectCatFirmByFirmId(String firmId)
    {
        return catFirmMapper.selectCatFirmByFirmId(firmId);
    }

    /**
     * 查询公司表列表
     * 
     * @param catFirm 公司表
     * @return 公司表
     */
    @Override
    public List<CatFirm> selectCatFirmList(CatFirm catFirm)
    {
        return catFirmMapper.selectCatFirmList(catFirm);
    }

    /**
     * 新增公司表
     * 
     * @param catFirm 公司表
     * @return 结果
     */
    @Override
    public int insertCatFirm(CatFirm catFirm)
    {
        catFirm.setCreateTime(DateUtils.getNowDate());
        return catFirmMapper.insertCatFirm(catFirm);
    }

    /**
     * 修改公司表
     * 
     * @param catFirm 公司表
     * @return 结果
     */
    @Override
    public int updateCatFirm(CatFirm catFirm)
    {
        catFirm.setUpdateTime(DateUtils.getNowDate());
        return catFirmMapper.updateCatFirm(catFirm);
    }

    /**
     * 批量删除公司表
     * 
     * @param firmIds 需要删除的公司表主键
     * @return 结果
     */
    @Override
    public int deleteCatFirmByFirmIds(String[] firmIds)
    {
        return catFirmMapper.deleteCatFirmByFirmIds(firmIds);
    }

    /**
     * 删除公司表信息
     * 
     * @param firmId 公司表主键
     * @return 结果
     */
    @Override
    public int deleteCatFirmByFirmId(String firmId)
    {
        return catFirmMapper.deleteCatFirmByFirmId(firmId);
    }
}
