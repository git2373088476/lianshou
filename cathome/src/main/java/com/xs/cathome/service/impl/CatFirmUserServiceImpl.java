package com.xs.cathome.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xs.cathome.mapper.CatFirmUserMapper;
import com.xs.cathome.domain.CatFirmUser;
import com.xs.cathome.service.ICatFirmUserService;

/**
 * 公司员工表Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-15
 */
@Service
public class CatFirmUserServiceImpl implements ICatFirmUserService 
{
    @Autowired
    private CatFirmUserMapper catFirmUserMapper;

    /**
     * 查询公司员工表
     * 
     * @param id 公司员工表主键
     * @return 公司员工表
     */
    @Override
    public CatFirmUser selectCatFirmUserById(String id)
    {
        return catFirmUserMapper.selectCatFirmUserById(id);
    }

    /**
     * 查询公司员工表列表
     * 
     * @param catFirmUser 公司员工表
     * @return 公司员工表
     */
    @Override
    public List<CatFirmUser> selectCatFirmUserList(CatFirmUser catFirmUser)
    {
        return catFirmUserMapper.selectCatFirmUserList(catFirmUser);
    }

    /**
     * 新增公司员工表
     * 
     * @param catFirmUser 公司员工表
     * @return 结果
     */
    @Override
    public int insertCatFirmUser(CatFirmUser catFirmUser)
    {
        catFirmUser.setCreateTime(DateUtils.getNowDate());
        return catFirmUserMapper.insertCatFirmUser(catFirmUser);
    }

    /**
     * 修改公司员工表
     * 
     * @param catFirmUser 公司员工表
     * @return 结果
     */
    @Override
    public int updateCatFirmUser(CatFirmUser catFirmUser)
    {
        catFirmUser.setUpdateTime(DateUtils.getNowDate());
        return catFirmUserMapper.updateCatFirmUser(catFirmUser);
    }

    /**
     * 批量删除公司员工表
     * 
     * @param ids 需要删除的公司员工表主键
     * @return 结果
     */
    @Override
    public int deleteCatFirmUserByIds(String[] ids)
    {
        return catFirmUserMapper.deleteCatFirmUserByIds(ids);
    }

    /**
     * 删除公司员工表信息
     * 
     * @param id 公司员工表主键
     * @return 结果
     */
    @Override
    public int deleteCatFirmUserById(String id)
    {
        return catFirmUserMapper.deleteCatFirmUserById(id);
    }
}
