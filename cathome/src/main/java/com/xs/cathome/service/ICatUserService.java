package com.xs.cathome.service;

import com.xs.cathome.domain.CatUser;

import java.util.List;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2023-03-13
 */
public interface ICatUserService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param userId 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public CatUser selectCatUserByUserId(String userId);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param catUser 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<CatUser> selectCatUserList(CatUser catUser);

    /**
     * 新增【请填写功能名称】
     * 
     * @param catUser 【请填写功能名称】
     * @return 结果
     */
    public int insertCatUser(CatUser catUser);

    /**
     * 修改【请填写功能名称】
     * 
     * @param catUser 【请填写功能名称】
     * @return 结果
     */
    public int updateCatUser(CatUser catUser);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param userIds 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteCatUserByUserIds(Long[] userIds);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param userId 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteCatUserByUserId(Long userId);
}
