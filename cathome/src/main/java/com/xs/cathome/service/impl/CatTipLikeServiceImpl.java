package com.xs.cathome.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xs.cathome.mapper.CatTipLikeMapper;
import com.xs.cathome.domain.CatTipLike;
import com.xs.cathome.service.ICatTipLikeService;

/**
 * 打赏点赞Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-14
 */
@Service
public class CatTipLikeServiceImpl implements ICatTipLikeService 
{
    @Autowired
    private CatTipLikeMapper catTipLikeMapper;

    /**
     * 查询打赏点赞
     * 
     * @param likeId 打赏点赞主键
     * @return 打赏点赞
     */
    @Override
    public CatTipLike selectCatTipLikeByLikeId(String likeId)
    {
        return catTipLikeMapper.selectCatTipLikeByLikeId(likeId);
    }

    /**
     * 查询打赏点赞列表
     * 
     * @param catTipLike 打赏点赞
     * @return 打赏点赞
     */
    @Override
    public List<CatTipLike> selectCatTipLikeList(CatTipLike catTipLike)
    {
        return catTipLikeMapper.selectCatTipLikeList(catTipLike);
    }

    /**
     * 新增打赏点赞
     * 
     * @param catTipLike 打赏点赞
     * @return 结果
     */
    @Override
    public int insertCatTipLike(CatTipLike catTipLike)
    {
        catTipLike.setCreateTime(DateUtils.getNowDate());
        return catTipLikeMapper.insertCatTipLike(catTipLike);
    }

    /**
     * 修改打赏点赞
     * 
     * @param catTipLike 打赏点赞
     * @return 结果
     */
    @Override
    public int updateCatTipLike(CatTipLike catTipLike)
    {
        catTipLike.setUpdateTime(DateUtils.getNowDate());
        return catTipLikeMapper.updateCatTipLike(catTipLike);
    }

    /**
     * 批量删除打赏点赞
     * 
     * @param likeIds 需要删除的打赏点赞主键
     * @return 结果
     */
    @Override
    public int deleteCatTipLikeByLikeIds(String[] likeIds)
    {
        return catTipLikeMapper.deleteCatTipLikeByLikeIds(likeIds);
    }

    /**
     * 删除打赏点赞信息
     * 
     * @param likeId 打赏点赞主键
     * @return 结果
     */
    @Override
    public int deleteCatTipLikeByLikeId(String likeId)
    {
        return catTipLikeMapper.deleteCatTipLikeByLikeId(likeId);
    }
}
