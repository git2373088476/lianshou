package com.xs.cathome.service.impl;

import java.util.List;

import com.ruoyi.common.utils.uuid.IdUtils;
import com.xs.cathome.domain.CatUserConcern;
import com.xs.cathome.mapper.CatUserConcernMapper;
import com.xs.cathome.service.ICatUserConcernService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-13
 */
@Service
public class CatUserConcernServiceImpl implements ICatUserConcernService
{
    @Autowired
    private CatUserConcernMapper catUserConcernMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public CatUserConcern selectCatUserConcernById(String id)
    {
        return catUserConcernMapper.selectCatUserConcernById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param catUserConcern 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<CatUserConcern> selectCatUserConcernList(CatUserConcern catUserConcern)
    {
        return catUserConcernMapper.selectCatUserConcernList(catUserConcern);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param catUserConcern 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertCatUserConcern(CatUserConcern catUserConcern)
    {
        return catUserConcernMapper.insertCatUserConcern(catUserConcern);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param catUserConcern 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateCatUserConcern(CatUserConcern catUserConcern)
    {
        return catUserConcernMapper.updateCatUserConcern(catUserConcern);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteCatUserConcernByIds(Long[] ids)
    {
        return catUserConcernMapper.deleteCatUserConcernByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteCatUserConcernById(Long id)
    {
        return catUserConcernMapper.deleteCatUserConcernById(id);
    }
}
