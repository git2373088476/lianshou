package com.xs.cathome.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import com.xs.cathome.domain.CatImg;
import com.xs.cathome.mapper.CatVoteMapper;
import com.xs.cathome.domain.CatVote;
import com.xs.cathome.service.ICatVoteService;

/**
 * CatvoteService业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-17
 */
@Service
public class CatVoteServiceImpl implements ICatVoteService 
{
    @Autowired
    private CatVoteMapper catVoteMapper;

    /**
     * 查询Catvote
     * 
     * @param voteId Catvote主键
     * @return Catvote
     */
    @Override
    public CatVote selectCatVoteByVoteId(String voteId)
    {
        return catVoteMapper.selectCatVoteByVoteId(voteId);
    }

    /**
     * 查询Catvote列表
     * 
     * @param catVote Catvote
     * @return Catvote
     */
    @Override
    public List<CatVote> selectCatVoteList(CatVote catVote)
    {
        return catVoteMapper.selectCatVoteList(catVote);
    }

    /**
     * 新增Catvote
     * 
     * @param catVote Catvote
     * @return 结果
     */
    @Transactional
    @Override
    public int insertCatVote(CatVote catVote)
    {
        catVote.setCreateTime(DateUtils.getNowDate());
        int rows = catVoteMapper.insertCatVote(catVote);
        insertCatImg(catVote);
        return rows;
    }

    /**
     * 修改Catvote
     * 
     * @param catVote Catvote
     * @return 结果
     */
    @Transactional
    @Override
    public int updateCatVote(CatVote catVote)
    {
        catVoteMapper.deleteCatImgByImgId(catVote.getVoteId());
        insertCatImg(catVote);
        return catVoteMapper.updateCatVote(catVote);
    }

    /**
     * 批量删除Catvote
     * 
     * @param voteIds 需要删除的Catvote主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteCatVoteByVoteIds(String[] voteIds)
    {
        catVoteMapper.deleteCatImgByImgIds(voteIds);
        return catVoteMapper.deleteCatVoteByVoteIds(voteIds);
    }

    /**
     * 删除Catvote信息
     * 
     * @param voteId Catvote主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteCatVoteByVoteId(String voteId)
    {
        catVoteMapper.deleteCatImgByImgId(voteId);
        return catVoteMapper.deleteCatVoteByVoteId(voteId);
    }

    /**
     * 新增文章图片 评论图片信息
     * 
     * @param catVote Catvote对象
     */
    public void insertCatImg(CatVote catVote)
    {
        List<CatImg> catImgList = catVote.getCatImgList();
        String voteId = catVote.getVoteId();
        if (StringUtils.isNotNull(catImgList))
        {
            List<CatImg> list = new ArrayList<CatImg>();
            for (CatImg catImg : catImgList)
            {
                catImg.setImgId(voteId);
                list.add(catImg);
            }
            if (list.size() > 0)
            {
                catVoteMapper.batchCatImg(list);
            }
        }
    }
}
