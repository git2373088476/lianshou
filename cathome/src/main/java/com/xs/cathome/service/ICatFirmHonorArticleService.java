package com.xs.cathome.service;

import java.util.List;
import com.xs.cathome.domain.CatFirmHonorArticle;

/**
 * 公司荣誉和评论Service接口
 * 
 * @author ruoyi
 * @date 2023-03-15
 */
public interface ICatFirmHonorArticleService 
{
    /**
     * 查询公司荣誉和评论
     * 
     * @param artHonorId 公司荣誉和评论主键
     * @return 公司荣誉和评论
     */
    public CatFirmHonorArticle selectCatFirmHonorArticleByArtHonorId(String artHonorId);

    /**
     * 查询公司荣誉和评论列表
     * 
     * @param catFirmHonorArticle 公司荣誉和评论
     * @return 公司荣誉和评论集合
     */
    public List<CatFirmHonorArticle> selectCatFirmHonorArticleList(CatFirmHonorArticle catFirmHonorArticle);

    /**
     * 新增公司荣誉和评论
     * 
     * @param catFirmHonorArticle 公司荣誉和评论
     * @return 结果
     */
    public int insertCatFirmHonorArticle(CatFirmHonorArticle catFirmHonorArticle);

    /**
     * 修改公司荣誉和评论
     * 
     * @param catFirmHonorArticle 公司荣誉和评论
     * @return 结果
     */
    public int updateCatFirmHonorArticle(CatFirmHonorArticle catFirmHonorArticle);

    /**
     * 批量删除公司荣誉和评论
     * 
     * @param artHonorIds 需要删除的公司荣誉和评论主键集合
     * @return 结果
     */
    public int deleteCatFirmHonorArticleByArtHonorIds(String[] artHonorIds);

    /**
     * 删除公司荣誉和评论信息
     * 
     * @param artHonorId 公司荣誉和评论主键
     * @return 结果
     */
    public int deleteCatFirmHonorArticleByArtHonorId(String artHonorId);

    CatFirmHonorArticle selectCatFirmHonorArticleByHonorId(String honorId);
}
