package com.xs.cathome.service;

import com.xs.cathome.domain.CatUserBlacklist;

import java.util.List;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2023-03-13
 */
public interface ICatUserBlacklistService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public CatUserBlacklist selectCatUserBlacklistById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param catUserBlacklist 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<CatUserBlacklist> selectCatUserBlacklistList(CatUserBlacklist catUserBlacklist);

    /**
     * 新增【请填写功能名称】
     * 
     * @param catUserBlacklist 【请填写功能名称】
     * @return 结果
     */
    public int insertCatUserBlacklist(CatUserBlacklist catUserBlacklist);

    /**
     * 修改【请填写功能名称】
     * 
     * @param catUserBlacklist 【请填写功能名称】
     * @return 结果
     */
    public int updateCatUserBlacklist(CatUserBlacklist catUserBlacklist);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteCatUserBlacklistByIds(Long[] ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteCatUserBlacklistById(Long id);
}
