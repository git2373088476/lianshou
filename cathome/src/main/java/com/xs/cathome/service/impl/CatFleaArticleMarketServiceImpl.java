package com.xs.cathome.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xs.cathome.mapper.CatFleaArticleMarketMapper;
import com.xs.cathome.domain.CatFleaArticleMarket;
import com.xs.cathome.service.ICatFleaArticleMarketService;

/**
 * 跳蚤信息关联表Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-16
 */
@Service
public class CatFleaArticleMarketServiceImpl implements ICatFleaArticleMarketService 
{
    @Autowired
    private CatFleaArticleMarketMapper catFleaArticleMarketMapper;

    /**
     * 查询跳蚤信息关联表
     * 
     * @param artMarId 跳蚤信息关联表主键
     * @return 跳蚤信息关联表
     */
    @Override
    public CatFleaArticleMarket selectCatFleaArticleMarketByArtMarId(String artMarId)
    {
        return catFleaArticleMarketMapper.selectCatFleaArticleMarketByArtMarId(artMarId);
    }

    /**
     * 查询跳蚤信息关联表列表
     * 
     * @param catFleaArticleMarket 跳蚤信息关联表
     * @return 跳蚤信息关联表
     */
    @Override
    public List<CatFleaArticleMarket> selectCatFleaArticleMarketList(CatFleaArticleMarket catFleaArticleMarket)
    {
        return catFleaArticleMarketMapper.selectCatFleaArticleMarketList(catFleaArticleMarket);
    }

    /**
     * 新增跳蚤信息关联表
     * 
     * @param catFleaArticleMarket 跳蚤信息关联表
     * @return 结果
     */
    @Override
    public int insertCatFleaArticleMarket(CatFleaArticleMarket catFleaArticleMarket)
    {
        return catFleaArticleMarketMapper.insertCatFleaArticleMarket(catFleaArticleMarket);
    }

    /**
     * 修改跳蚤信息关联表
     * 
     * @param catFleaArticleMarket 跳蚤信息关联表
     * @return 结果
     */
    @Override
    public int updateCatFleaArticleMarket(CatFleaArticleMarket catFleaArticleMarket)
    {
        return catFleaArticleMarketMapper.updateCatFleaArticleMarket(catFleaArticleMarket);
    }

    /**
     * 批量删除跳蚤信息关联表
     * 
     * @param artMarIds 需要删除的跳蚤信息关联表主键
     * @return 结果
     */
    @Override
    public int deleteCatFleaArticleMarketByArtMarIds(String[] artMarIds)
    {
        return catFleaArticleMarketMapper.deleteCatFleaArticleMarketByArtMarIds(artMarIds);
    }

    /**
     * 删除跳蚤信息关联表信息
     * 
     * @param artMarId 跳蚤信息关联表主键
     * @return 结果
     */
    @Override
    public int deleteCatFleaArticleMarketByArtMarId(String artMarId)
    {
        return catFleaArticleMarketMapper.deleteCatFleaArticleMarketByArtMarId(artMarId);
    }
}
