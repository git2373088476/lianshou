package com.xs.cathome.service;

import java.util.List;
import com.xs.cathome.domain.CatTipCollection;

/**
 * 打赏收藏Service接口
 * 
 * @author ruoyi
 * @date 2023-03-14
 */
public interface ICatTipCollectionService 
{
    /**
     * 查询打赏收藏
     * 
     * @param collectionId 打赏收藏主键
     * @return 打赏收藏
     */
    public CatTipCollection selectCatTipCollectionByCollectionId(String collectionId);

    /**
     * 查询打赏收藏列表
     * 
     * @param catTipCollection 打赏收藏
     * @return 打赏收藏集合
     */
    public List<CatTipCollection> selectCatTipCollectionList(CatTipCollection catTipCollection);

    /**
     * 新增打赏收藏
     * 
     * @param catTipCollection 打赏收藏
     * @return 结果
     */
    public int insertCatTipCollection(CatTipCollection catTipCollection);

    /**
     * 修改打赏收藏
     * 
     * @param catTipCollection 打赏收藏
     * @return 结果
     */
    public int updateCatTipCollection(CatTipCollection catTipCollection);

    /**
     * 批量删除打赏收藏
     * 
     * @param collectionIds 需要删除的打赏收藏主键集合
     * @return 结果
     */
    public int deleteCatTipCollectionByCollectionIds(String[] collectionIds);

    /**
     * 删除打赏收藏信息
     * 
     * @param collectionId 打赏收藏主键
     * @return 结果
     */
    public int deleteCatTipCollectionByCollectionId(String collectionId);
}
