package com.xs.cathome.service;

import java.util.List;
import com.xs.cathome.domain.CatFleaMarket;
import com.xs.cathome.domain.vo.CatFleaMarketVO;

/**
 * 跳蚤市场Service接口
 * 
 * @author ruoyi
 * @date 2023-03-16
 */
public interface ICatFleaMarketService 
{
    /**
     * 查询跳蚤市场
     * 
     * @param marketId 跳蚤市场主键
     * @return 跳蚤市场
     */
    public CatFleaMarket selectCatFleaMarketByMarketId(String marketId);

    /**
     * 查询跳蚤市场列表
     * 
     * @param catFleaMarket 跳蚤市场
     * @return 跳蚤市场集合
     */
    public List<CatFleaMarket> selectCatFleaMarketList(CatFleaMarket catFleaMarket);

    /**
     * 新增跳蚤市场
     * 
     * @param catFleaMarket 跳蚤市场
     * @return 结果
     */
    public int insertCatFleaMarket(CatFleaMarket catFleaMarket);

    /**
     * 修改跳蚤市场
     * 
     * @param catFleaMarket 跳蚤市场
     * @return 结果
     */
    public int updateCatFleaMarket(CatFleaMarket catFleaMarket);

    /**
     * 批量删除跳蚤市场
     * 
     * @param marketIds 需要删除的跳蚤市场主键集合
     * @return 结果
     */
    public int deleteCatFleaMarketByMarketIds(String[] marketIds);

    /**
     * 删除跳蚤市场信息
     * 
     * @param marketId 跳蚤市场主键
     * @return 结果
     */
    public int deleteCatFleaMarketByMarketId(String marketId);

    int addmarket(CatFleaMarketVO catFleaMarketVO);

    List<CatFleaMarketVO> selectMarketList(CatFleaMarketVO catFleaMarketVO);
}
