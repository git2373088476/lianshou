package com.xs.cathome.service.impl;

import java.util.List;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xs.cathome.mapper.CatTipArticleMapper;
import com.xs.cathome.domain.CatTipArticle;
import com.xs.cathome.service.ICatTipArticleService;

/**
 * 打赏发布表Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-14
 */
@Service
public class CatTipArticleServiceImpl implements ICatTipArticleService 
{
    @Autowired
    private CatTipArticleMapper catTipArticleMapper;

    /**
     * 查询打赏发布表
     * 
     * @param articleId 打赏发布表主键
     * @return 打赏发布表
     */
    @Override
    public CatTipArticle selectCatTipArticleByArticleId(String articleId)
    {
        return catTipArticleMapper.selectCatTipArticleByArticleId(articleId);
    }

    /**
     * 查询打赏发布表列表
     * 
     * @param catTipArticle 打赏发布表
     * @return 打赏发布表
     */
    @Override
    public List<CatTipArticle> selectCatTipArticleList(CatTipArticle catTipArticle)
    {
        return catTipArticleMapper.selectCatTipArticleList(catTipArticle);
    }

    /**
     * 新增打赏发布表
     * 
     * @param catTipArticle 打赏发布表
     * @return 结果
     */
    @Override
    public int insertCatTipArticle(CatTipArticle catTipArticle)
    {
        catTipArticle.setArticleId(IdUtils.simpleUUID());
        catTipArticle.setCreateTime(DateUtils.getNowDate());
        return catTipArticleMapper.insertCatTipArticle(catTipArticle);
    }

    /**
     * 新增打赏发布表
     *
     * @param catTipArticle 打赏发布表
     * @return 结果
     */
    @Override
    public String insertCatTipArticleReturnID(CatTipArticle catTipArticle)
    {
        catTipArticle.setArticleId(IdUtils.simpleUUID());
        catTipArticle.setCreateTime(DateUtils.getNowDate());
        catTipArticleMapper.insertCatTipArticle(catTipArticle);
        return catTipArticle.getArticleId();
    }

    /**
     * 修改打赏发布表
     * 
     * @param catTipArticle 打赏发布表
     * @return 结果
     */
    @Override
    public int updateCatTipArticle(CatTipArticle catTipArticle)
    {
        catTipArticle.setUpdateTime(DateUtils.getNowDate());
        return catTipArticleMapper.updateCatTipArticle(catTipArticle);
    }

    /**
     * 批量删除打赏发布表
     * 
     * @param articleIds 需要删除的打赏发布表主键
     * @return 结果
     */
    @Override
    public int deleteCatTipArticleByArticleIds(String[] articleIds)
    {
        return catTipArticleMapper.deleteCatTipArticleByArticleIds(articleIds);
    }

    /**
     * 删除打赏发布表信息
     * 
     * @param articleId 打赏发布表主键
     * @return 结果
     */
    @Override
    public int deleteCatTipArticleByArticleId(String articleId)
    {
        return catTipArticleMapper.deleteCatTipArticleByArticleId(articleId);
    }

    /**
     * 按照最新、推荐、最热查询打赏发布列表
     * @param status
     * @return
     */
    @Override
    public List<CatTipArticle> selectCatTipArticleList(String status) {
        if (status==null){
            //如果状态status为空，则调用selectCatTipArticleList（）返回查询数据
            List<CatTipArticle> catTipArticles = selectCatTipArticleList(new CatTipArticle());
            return catTipArticles;
        }else if ("0".equals(status)){
            //若status为0，则为推荐
            List<CatTipArticle> catTipArticles = catTipArticleMapper.selectRecommendList();
            return catTipArticles;
        }else if ("1".equals(status)){
            //若status为1，则为最新
            List<CatTipArticle> catTipArticles = catTipArticleMapper.selectDateList();
            return catTipArticles;
        }else if ("2".equals(status)){
            //若status为2，则为最热
            List<CatTipArticle> catTipArticles = catTipArticleMapper.selectHotList();
            return catTipArticles;
        }
        return null;
    }

    @Override
    public CatTipArticle selectCatTipArticleByArticleIdAndUserId(String articleId, String userId) {
        return catTipArticleMapper.selectCatTipArticleByArticleIdAndUserId(articleId,userId);
    }
}
