package com.xs.cathome.service;

import java.util.List;
import com.xs.cathome.domain.CatTipLike;

/**
 * 打赏点赞Service接口
 * 
 * @author ruoyi
 * @date 2023-03-14
 */
public interface ICatTipLikeService 
{
    /**
     * 查询打赏点赞
     * 
     * @param likeId 打赏点赞主键
     * @return 打赏点赞
     */
    public CatTipLike selectCatTipLikeByLikeId(String likeId);

    /**
     * 查询打赏点赞列表
     * 
     * @param catTipLike 打赏点赞
     * @return 打赏点赞集合
     */
    public List<CatTipLike> selectCatTipLikeList(CatTipLike catTipLike);

    /**
     * 新增打赏点赞
     * 
     * @param catTipLike 打赏点赞
     * @return 结果
     */
    public int insertCatTipLike(CatTipLike catTipLike);

    /**
     * 修改打赏点赞
     * 
     * @param catTipLike 打赏点赞
     * @return 结果
     */
    public int updateCatTipLike(CatTipLike catTipLike);

    /**
     * 批量删除打赏点赞
     * 
     * @param likeIds 需要删除的打赏点赞主键集合
     * @return 结果
     */
    public int deleteCatTipLikeByLikeIds(String[] likeIds);

    /**
     * 删除打赏点赞信息
     * 
     * @param likeId 打赏点赞主键
     * @return 结果
     */
    public int deleteCatTipLikeByLikeId(String likeId);
}
