package com.xs.cathome.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xs.cathome.mapper.CatCommentMapper;
import com.xs.cathome.domain.CatComment;
import com.xs.cathome.service.ICatCommentService;

/**
 * 评论Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-14
 */
@Service
public class CatCommentServiceImpl implements ICatCommentService 
{
    @Autowired
    private CatCommentMapper catCommentMapper;

    /**
     * 查询评论
     * 
     * @param commentId 评论主键
     * @return 评论
     */
    @Override
    public CatComment selectCatCommentByCommentId(String commentId)
    {
        return catCommentMapper.selectCatCommentByCommentId(commentId);
    }

    /**
     * 查询评论列表
     * 
     * @param catComment 评论
     * @return 评论
     */
    @Override
    public List<CatComment> selectCatCommentList(CatComment catComment)
    {
        return catCommentMapper.selectCatCommentList(catComment);
    }

    /**
     * 新增评论
     * 
     * @param catComment 评论
     * @return 结果
     */
    @Override
    public int insertCatComment(CatComment catComment)
    {
        return catCommentMapper.insertCatComment(catComment);
    }

    /**
     * 修改评论
     * 
     * @param catComment 评论
     * @return 结果
     */
    @Override
    public int updateCatComment(CatComment catComment)
    {
        return catCommentMapper.updateCatComment(catComment);
    }

    /**
     * 批量删除评论
     * 
     * @param commentIds 需要删除的评论主键
     * @return 结果
     */
    @Override
    public int deleteCatCommentByCommentIds(String[] commentIds)
    {
        return catCommentMapper.deleteCatCommentByCommentIds(commentIds);
    }

    /**
     * 删除评论信息
     * 
     * @param commentId 评论主键
     * @return 结果
     */
    @Override
    public int deleteCatCommentByCommentId(String commentId)
    {
        return catCommentMapper.deleteCatCommentByCommentId(commentId);
    }
}
