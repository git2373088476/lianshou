package com.xs.cathome.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xs.cathome.mapper.CatPrizeMapper;
import com.xs.cathome.domain.CatPrize;
import com.xs.cathome.service.ICatPrizeService;

/**
 * 奖品Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-14
 */
@Service
public class CatPrizeServiceImpl implements ICatPrizeService 
{
    @Autowired
    private CatPrizeMapper catPrizeMapper;

    /**
     * 查询奖品
     * 
     * @param articleId 奖品主键
     * @return 奖品
     */
    @Override
    public CatPrize selectCatPrizeByArticleId(String articleId)
    {
        return catPrizeMapper.selectCatPrizeByArticleId(articleId);
    }

    /**
     * 查询奖品列表
     * 
     * @param catPrize 奖品
     * @return 奖品
     */
    @Override
    public List<CatPrize> selectCatPrizeList(CatPrize catPrize)
    {
        return catPrizeMapper.selectCatPrizeList(catPrize);
    }

    /**
     * 新增奖品
     * 
     * @param catPrize 奖品
     * @return 结果
     */
    @Override
    public int insertCatPrize(CatPrize catPrize)
    {
        return catPrizeMapper.insertCatPrize(catPrize);
    }

    /**
     * 修改奖品
     * 
     * @param catPrize 奖品
     * @return 结果
     */
    @Override
    public int updateCatPrize(CatPrize catPrize)
    {
        return catPrizeMapper.updateCatPrize(catPrize);
    }

    /**
     * 批量删除奖品
     * 
     * @param articleIds 需要删除的奖品主键
     * @return 结果
     */
    @Override
    public int deleteCatPrizeByArticleIds(String[] articleIds)
    {
        return catPrizeMapper.deleteCatPrizeByArticleIds(articleIds);
    }

    /**
     * 删除奖品信息
     * 
     * @param articleId 奖品主键
     * @return 结果
     */
    @Override
    public int deleteCatPrizeByArticleId(String articleId)
    {
        return catPrizeMapper.deleteCatPrizeByArticleId(articleId);
    }
}
