package com.xs.cathome.mapper;

import java.util.List;
import com.xs.cathome.domain.CatFleaMarket;
import org.apache.ibatis.annotations.Mapper;

/**
 * 跳蚤市场Mapper接口
 * 
 * @author ruoyi
 * @date 2023-03-16
 */
@Mapper
public interface CatFleaMarketMapper 
{
    /**
     * 查询跳蚤市场
     * 
     * @param marketId 跳蚤市场主键
     * @return 跳蚤市场
     */
    public CatFleaMarket selectCatFleaMarketByMarketId(String marketId);

    /**
     * 查询跳蚤市场列表
     * 
     * @param catFleaMarket 跳蚤市场
     * @return 跳蚤市场集合
     */
    public List<CatFleaMarket> selectCatFleaMarketList(CatFleaMarket catFleaMarket);

    /**
     * 新增跳蚤市场
     * 
     * @param catFleaMarket 跳蚤市场
     * @return 结果
     */
    public int insertCatFleaMarket(CatFleaMarket catFleaMarket);

    /**
     * 修改跳蚤市场
     * 
     * @param catFleaMarket 跳蚤市场
     * @return 结果
     */
    public int updateCatFleaMarket(CatFleaMarket catFleaMarket);

    /**
     * 删除跳蚤市场
     * 
     * @param marketId 跳蚤市场主键
     * @return 结果
     */
    public int deleteCatFleaMarketByMarketId(String marketId);

    /**
     * 批量删除跳蚤市场
     * 
     * @param marketIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCatFleaMarketByMarketIds(String[] marketIds);
}
