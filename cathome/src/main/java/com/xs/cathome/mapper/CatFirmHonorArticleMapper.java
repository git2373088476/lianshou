package com.xs.cathome.mapper;

import java.util.List;
import com.xs.cathome.domain.CatFirmHonorArticle;
import org.apache.ibatis.annotations.Param;

/**
 * 公司荣誉和评论Mapper接口
 * 
 * @author ruoyi
 * @date 2023-03-15
 */
public interface CatFirmHonorArticleMapper 
{
    /**
     * 查询公司荣誉和评论
     * 
     * @param artHonorId 公司荣誉和评论主键
     * @return 公司荣誉和评论
     */
    public CatFirmHonorArticle selectCatFirmHonorArticleByArtHonorId(String artHonorId);

    /**
     * 查询公司荣誉和评论列表
     * 
     * @param catFirmHonorArticle 公司荣誉和评论
     * @return 公司荣誉和评论集合
     */
    public List<CatFirmHonorArticle> selectCatFirmHonorArticleList(CatFirmHonorArticle catFirmHonorArticle);

    /**
     * 新增公司荣誉和评论
     * 
     * @param catFirmHonorArticle 公司荣誉和评论
     * @return 结果
     */
    public int insertCatFirmHonorArticle(CatFirmHonorArticle catFirmHonorArticle);

    /**
     * 修改公司荣誉和评论
     * 
     * @param catFirmHonorArticle 公司荣誉和评论
     * @return 结果
     */
    public int updateCatFirmHonorArticle(CatFirmHonorArticle catFirmHonorArticle);

    /**
     * 删除公司荣誉和评论
     * 
     * @param artHonorId 公司荣誉和评论主键
     * @return 结果
     */
    public int deleteCatFirmHonorArticleByArtHonorId(String artHonorId);

    /**
     * 批量删除公司荣誉和评论
     * 
     * @param artHonorIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCatFirmHonorArticleByArtHonorIds(String[] artHonorIds);

    CatFirmHonorArticle selectCatFirmHonorArticleByHonorId(@Param("honorId") String honorId);
}
