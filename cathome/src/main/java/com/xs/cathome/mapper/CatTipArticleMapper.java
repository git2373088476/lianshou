package com.xs.cathome.mapper;

import java.util.List;
import com.xs.cathome.domain.CatTipArticle;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 打赏发布表Mapper接口
 * 
 * @author ruoyi
 * @date 2023-03-14
 */
@Mapper
public interface CatTipArticleMapper 
{
    /**
     * 查询打赏发布表
     * 
     * @param articleId 打赏发布表主键
     * @return 打赏发布表
     */
    public CatTipArticle selectCatTipArticleByArticleId(String articleId);

    /**
     * 查询打赏发布表列表
     * 
     * @param catTipArticle 打赏发布表
     * @return 打赏发布表集合
     */
    public List<CatTipArticle> selectCatTipArticleList(CatTipArticle catTipArticle);

    /**
     * 新增打赏发布表
     * 
     * @param catTipArticle 打赏发布表
     * @return 结果
     */
    public int insertCatTipArticle(CatTipArticle catTipArticle);

    /**
     * 修改打赏发布表
     * 
     * @param catTipArticle 打赏发布表
     * @return 结果
     */
    public int updateCatTipArticle(CatTipArticle catTipArticle);

    /**
     * 删除打赏发布表
     * 
     * @param articleId 打赏发布表主键
     * @return 结果
     */
    public int deleteCatTipArticleByArticleId(String articleId);

    /**
     * 批量删除打赏发布表
     * 
     * @param articleIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCatTipArticleByArticleIds(String[] articleIds);

    List<CatTipArticle> selectRecommendList();

    List<CatTipArticle> selectDateList();

    List<CatTipArticle> selectHotList();

    CatTipArticle selectCatTipArticleByArticleIdAndUserId(@Param("articleId") String articleId, @Param("userId") String userId);
}
