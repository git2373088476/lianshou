package com.xs.cathome.mapper;

import java.util.List;
import com.xs.cathome.domain.CatComment;
import org.apache.ibatis.annotations.Mapper;

/**
 * 评论Mapper接口
 * 
 * @author ruoyi
 * @date 2023-03-14
 */
@Mapper
public interface CatCommentMapper 
{
    /**
     * 查询评论
     * 
     * @param commentId 评论主键
     * @return 评论
     */
    public CatComment selectCatCommentByCommentId(String commentId);

    /**
     * 查询评论列表
     * 
     * @param catComment 评论
     * @return 评论集合
     */
    public List<CatComment> selectCatCommentList(CatComment catComment);

    /**
     * 新增评论
     * 
     * @param catComment 评论
     * @return 结果
     */
    public int insertCatComment(CatComment catComment);

    /**
     * 修改评论
     * 
     * @param catComment 评论
     * @return 结果
     */
    public int updateCatComment(CatComment catComment);

    /**
     * 删除评论
     * 
     * @param commentId 评论主键
     * @return 结果
     */
    public int deleteCatCommentByCommentId(String commentId);

    /**
     * 批量删除评论
     * 
     * @param commentIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCatCommentByCommentIds(String[] commentIds);
}
