package com.xs.cathome.mapper;

import java.util.List;
import com.xs.cathome.domain.CatPrize;

/**
 * 奖品Mapper接口
 * 
 * @author ruoyi
 * @date 2023-03-14
 */
public interface CatPrizeMapper 
{
    /**
     * 查询奖品
     * 
     * @param articleId 奖品主键
     * @return 奖品
     */
    public CatPrize selectCatPrizeByArticleId(String articleId);

    /**
     * 查询奖品列表
     * 
     * @param catPrize 奖品
     * @return 奖品集合
     */
    public List<CatPrize> selectCatPrizeList(CatPrize catPrize);

    /**
     * 新增奖品
     * 
     * @param catPrize 奖品
     * @return 结果
     */
    public int insertCatPrize(CatPrize catPrize);

    /**
     * 修改奖品
     * 
     * @param catPrize 奖品
     * @return 结果
     */
    public int updateCatPrize(CatPrize catPrize);

    /**
     * 删除奖品
     * 
     * @param articleId 奖品主键
     * @return 结果
     */
    public int deleteCatPrizeByArticleId(String articleId);

    /**
     * 批量删除奖品
     * 
     * @param articleIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCatPrizeByArticleIds(String[] articleIds);
}
