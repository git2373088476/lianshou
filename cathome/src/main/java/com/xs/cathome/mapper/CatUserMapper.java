package com.xs.cathome.mapper;

import com.xs.cathome.domain.CatUser;

import java.util.List;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2023-03-13
 */
public interface CatUserMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param userId 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public CatUser selectCatUserByUserId(String userId);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param catUser 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<CatUser> selectCatUserList(CatUser catUser);

    /**
     * 新增【请填写功能名称】
     * 
     * @param catUser 【请填写功能名称】
     * @return 结果
     */
    public int insertCatUser(CatUser catUser);

    /**
     * 修改【请填写功能名称】
     * 
     * @param catUser 【请填写功能名称】
     * @return 结果
     */
    public int updateCatUser(CatUser catUser);

    /**
     * 删除【请填写功能名称】
     * 
     * @param userId 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteCatUserByUserId(Long userId);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param userIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCatUserByUserIds(Long[] userIds);
}
