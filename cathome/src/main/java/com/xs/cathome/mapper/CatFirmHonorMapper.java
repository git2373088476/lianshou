package com.xs.cathome.mapper;

import java.util.List;
import com.xs.cathome.domain.CatFirmHonor;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 公司荣誉表Mapper接口
 * 
 * @author ruoyi
 * @date 2023-03-15
 */
@Mapper
public interface CatFirmHonorMapper 
{
    /**
     * 查询公司荣誉表
     * 
     * @param honorId 公司荣誉表主键
     * @return 公司荣誉表
     */
    public CatFirmHonor selectCatFirmHonorByHonorId(String honorId);

    /**
     * 查询公司荣誉表列表
     * 
     * @param catFirmHonor 公司荣誉表
     * @return 公司荣誉表集合
     */
    public List<CatFirmHonor> selectCatFirmHonorList(CatFirmHonor catFirmHonor);

    /**
     * 新增公司荣誉表
     * 
     * @param catFirmHonor 公司荣誉表
     * @return 结果
     */
    public int insertCatFirmHonor(CatFirmHonor catFirmHonor);

    /**
     * 修改公司荣誉表
     * 
     * @param catFirmHonor 公司荣誉表
     * @return 结果
     */
    public int updateCatFirmHonor(CatFirmHonor catFirmHonor);

    /**
     * 删除公司荣誉表
     * 
     * @param honorId 公司荣誉表主键
     * @return 结果
     */
    public int deleteCatFirmHonorByHonorId(String honorId);

    /**
     * 批量删除公司荣誉表
     * 
     * @param honorIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCatFirmHonorByHonorIds(String[] honorIds);

    List<CatFirmHonor> selectCatFirmHonorByFirmId(@Param("firmId") String firmId);
}
