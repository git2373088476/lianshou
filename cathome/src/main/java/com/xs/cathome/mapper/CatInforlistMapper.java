package com.xs.cathome.mapper;

import java.util.List;
import com.xs.cathome.domain.CatInforlist;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 资讯关系表Mapper接口
 * 
 * @author ruoyi
 * @date 2023-03-15
 */
@Mapper
public interface CatInforlistMapper 
{
    /**
     * 查询资讯关系表
     * 
     * @param inforlistId 资讯关系表主键
     * @return 资讯关系表
     */
    public CatInforlist selectCatInforlistByInforlistId(String inforlistId);

    /**
     * 查询资讯关系表列表
     * 
     * @param catInforlist 资讯关系表
     * @return 资讯关系表集合
     */
    public List<CatInforlist> selectCatInforlistList(CatInforlist catInforlist);

    /**
     * 新增资讯关系表
     * 
     * @param catInforlist 资讯关系表
     * @return 结果
     */
    public int insertCatInforlist(CatInforlist catInforlist);

    /**
     * 修改资讯关系表
     * 
     * @param catInforlist 资讯关系表
     * @return 结果
     */
    public int updateCatInforlist(CatInforlist catInforlist);

    /**
     * 删除资讯关系表
     * 
     * @param inforlistId 资讯关系表主键
     * @return 结果
     */
    public int deleteCatInforlistByInforlistId(String inforlistId);

    /**
     * 批量删除资讯关系表
     * 
     * @param inforlistIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCatInforlistByInforlistIds(String[] inforlistIds);

    List<CatInforlist> selectCatInforlistByInforTypeId(@Param("inforTypeId") String inforTypeId);
}
