package com.xs.cathome.mapper;

import java.util.List;
import com.xs.cathome.domain.CatTipReply;
import org.apache.ibatis.annotations.Param;

/**
 * 打赏评论Mapper接口
 * 
 * @author ruoyi
 * @date 2023-03-14
 */
public interface CatTipReplyMapper 
{
    /**
     * 查询打赏评论
     * 
     * @param userId 打赏评论主键
     * @return 打赏评论
     */
    public CatTipReply selectCatTipReplyByUserId(String userId);

    /**
     * 查询打赏评论列表
     * 
     * @param catTipReply 打赏评论
     * @return 打赏评论集合
     */
    public List<CatTipReply> selectCatTipReplyList(CatTipReply catTipReply);

    /**
     * 新增打赏评论
     * 
     * @param catTipReply 打赏评论
     * @return 结果
     */
    public int insertCatTipReply(CatTipReply catTipReply);

    /**
     * 修改打赏评论
     * 
     * @param catTipReply 打赏评论
     * @return 结果
     */
    public int updateCatTipReply(CatTipReply catTipReply);

    /**
     * 删除打赏评论
     * 
     * @param userId 打赏评论主键
     * @return 结果
     */
    public int deleteCatTipReplyByUserId(String userId);

    /**
     * 批量删除打赏评论
     * 
     * @param userIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCatTipReplyByUserIds(String[] userIds);

    List<CatTipReply> selectTipReplyList(@Param("articleId") String articleId);
}
