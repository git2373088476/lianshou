package com.xs.cathome.mapper;

import java.util.List;
import com.xs.cathome.domain.CatLikefavorites;

/**
 * 点赞收藏Mapper接口
 * 
 * @author ruoyi
 * @date 2023-03-14
 */
public interface CatLikefavoritesMapper 
{
    /**
     * 查询点赞收藏
     * 
     * @param id 点赞收藏主键
     * @return 点赞收藏
     */
    public CatLikefavorites selectCatLikefavoritesById(Long id);

    /**
     * 查询点赞收藏列表
     * 
     * @param catLikefavorites 点赞收藏
     * @return 点赞收藏集合
     */
    public List<CatLikefavorites> selectCatLikefavoritesList(CatLikefavorites catLikefavorites);

    /**
     * 新增点赞收藏
     * 
     * @param catLikefavorites 点赞收藏
     * @return 结果
     */
    public int insertCatLikefavorites(CatLikefavorites catLikefavorites);

    /**
     * 修改点赞收藏
     * 
     * @param catLikefavorites 点赞收藏
     * @return 结果
     */
    public int updateCatLikefavorites(CatLikefavorites catLikefavorites);

    /**
     * 删除点赞收藏
     * 
     * @param id 点赞收藏主键
     * @return 结果
     */
    public int deleteCatLikefavoritesById(Long id);

    /**
     * 批量删除点赞收藏
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCatLikefavoritesByIds(Long[] ids);
}
