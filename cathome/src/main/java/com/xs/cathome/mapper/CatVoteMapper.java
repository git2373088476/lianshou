package com.xs.cathome.mapper;

import java.util.List;
import com.xs.cathome.domain.CatVote;
import com.xs.cathome.domain.CatImg;

/**
 * CatvoteMapper接口
 * 
 * @author ruoyi
 * @date 2023-03-17
 */
public interface CatVoteMapper 
{
    /**
     * 查询Catvote
     * 
     * @param voteId Catvote主键
     * @return Catvote
     */
    public CatVote selectCatVoteByVoteId(String voteId);

    /**
     * 查询Catvote列表
     * 
     * @param catVote Catvote
     * @return Catvote集合
     */
    public List<CatVote> selectCatVoteList(CatVote catVote);

    /**
     * 新增Catvote
     * 
     * @param catVote Catvote
     * @return 结果
     */
    public int insertCatVote(CatVote catVote);

    /**
     * 修改Catvote
     * 
     * @param catVote Catvote
     * @return 结果
     */
    public int updateCatVote(CatVote catVote);

    /**
     * 删除Catvote
     * 
     * @param voteId Catvote主键
     * @return 结果
     */
    public int deleteCatVoteByVoteId(String voteId);

    /**
     * 批量删除Catvote
     * 
     * @param voteIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCatVoteByVoteIds(String[] voteIds);

    /**
     * 批量删除文章图片 评论图片
     * 
     * @param voteIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCatImgByImgIds(String[] voteIds);
    
    /**
     * 批量新增文章图片 评论图片
     * 
     * @param catImgList 文章图片 评论图片列表
     * @return 结果
     */
    public int batchCatImg(List<CatImg> catImgList);
    

    /**
     * 通过Catvote主键删除文章图片 评论图片信息
     * 
     * @param voteId CatvoteID
     * @return 结果
     */
    public int deleteCatImgByImgId(String voteId);
}
