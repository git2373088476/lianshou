package com.xs.cathome.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.xs.cathome.domain.vo.CatinfoVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.xs.cathome.domain.CatInforlist;
import com.xs.cathome.service.ICatInforlistService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 资讯关系表Controller
 * 
 * @author ruoyi
 * @date 2023-03-15
 */
@RestController
@RequestMapping("/system/inforlist")
public class CatInforlistController extends BaseController
{
    @Autowired
    private ICatInforlistService catInforlistService;

    /**
     * 按照类型查询资讯
     */
    @PreAuthorize("@ss.hasPermi('system:inforlist:query')")
    @GetMapping(value = "getInfoByType/{inforTypeId}")
    public TableDataInfo getInfoByType(@PathVariable("inforTypeId") String inforTypeId)
    {
        startPage();
        List<CatinfoVO> list = catInforlistService.getInfoByType(inforTypeId);
        return getDataTable(list);
    }

    /**
     * 新增资讯
     */
    @PreAuthorize("@ss.hasPermi('system:inforlist:addinfo')")
    @Log(title = "资讯关系表", businessType = BusinessType.INSERT)
    @PostMapping("/addinfo")
    public AjaxResult addinfo(@RequestBody CatinfoVO catinfoVO)
    {
        return toAjax(catInforlistService.insertCatInfor(catinfoVO));
    }

    /**
     * 查询资讯关系表列表
     */
    @PreAuthorize("@ss.hasPermi('system:inforlist:list')")
    @GetMapping("/list")
    public TableDataInfo list(CatInforlist catInforlist)
    {
        startPage();
        List<CatInforlist> list = catInforlistService.selectCatInforlistList(catInforlist);
        return getDataTable(list);
    }

    /**
     * 导出资讯关系表列表
     */
    @PreAuthorize("@ss.hasPermi('system:inforlist:export')")
    @Log(title = "资讯关系表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CatInforlist catInforlist)
    {
        List<CatInforlist> list = catInforlistService.selectCatInforlistList(catInforlist);
        ExcelUtil<CatInforlist> util = new ExcelUtil<CatInforlist>(CatInforlist.class);
        util.exportExcel(response, list, "资讯关系表数据");
    }

    /**
     * 获取资讯关系表详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:inforlist:query')")
    @GetMapping(value = "/{inforlistId}")
    public AjaxResult getInfo(@PathVariable("inforlistId") String inforlistId)
    {
        return success(catInforlistService.selectCatInforlistByInforlistId(inforlistId));
    }

    /**
     * 新增资讯关系表
     */
    @PreAuthorize("@ss.hasPermi('system:inforlist:add')")
    @Log(title = "资讯关系表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CatInforlist catInforlist)
    {
        return toAjax(catInforlistService.insertCatInforlist(catInforlist));
    }

    /**
     * 修改资讯关系表
     */
    @PreAuthorize("@ss.hasPermi('system:inforlist:edit')")
    @Log(title = "资讯关系表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CatInforlist catInforlist)
    {
        return toAjax(catInforlistService.updateCatInforlist(catInforlist));
    }

    /**
     * 删除资讯关系表
     */
    @PreAuthorize("@ss.hasPermi('system:inforlist:remove')")
    @Log(title = "资讯关系表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{inforlistIds}")
    public AjaxResult remove(@PathVariable String[] inforlistIds)
    {
        return toAjax(catInforlistService.deleteCatInforlistByInforlistIds(inforlistIds));
    }
}
