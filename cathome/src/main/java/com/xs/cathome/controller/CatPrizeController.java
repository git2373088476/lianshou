package com.xs.cathome.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.xs.cathome.domain.CatPrize;
import com.xs.cathome.service.ICatPrizeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 奖品Controller
 * 
 * @author ruoyi
 * @date 2023-03-14
 */
@RestController
@RequestMapping("/cathome/prize")
public class CatPrizeController extends BaseController
{
    @Autowired
    private ICatPrizeService catPrizeService;

    /**
     * 查询奖品列表
     */
    @PreAuthorize("@ss.hasPermi('system:prize:list')")
    @GetMapping("/list")
    public TableDataInfo list(CatPrize catPrize)
    {
        startPage();
        List<CatPrize> list = catPrizeService.selectCatPrizeList(catPrize);
        return getDataTable(list);
    }

    /**
     * 导出奖品列表
     */
    @PreAuthorize("@ss.hasPermi('system:prize:export')")
    @Log(title = "奖品", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CatPrize catPrize)
    {
        List<CatPrize> list = catPrizeService.selectCatPrizeList(catPrize);
        ExcelUtil<CatPrize> util = new ExcelUtil<CatPrize>(CatPrize.class);
        util.exportExcel(response, list, "奖品数据");
    }

    /**
     * 获取奖品详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:prize:query')")
    @GetMapping(value = "/{articleId}")
    public AjaxResult getInfo(@PathVariable("articleId") String articleId)
    {
        return success(catPrizeService.selectCatPrizeByArticleId(articleId));
    }

    /**
     * 新增奖品
     */
    @PreAuthorize("@ss.hasPermi('system:prize:add')")
    @Log(title = "奖品", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CatPrize catPrize)
    {
        return toAjax(catPrizeService.insertCatPrize(catPrize));
    }

    /**
     * 修改奖品
     */
    @PreAuthorize("@ss.hasPermi('system:prize:edit')")
    @Log(title = "奖品", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CatPrize catPrize)
    {
        return toAjax(catPrizeService.updateCatPrize(catPrize));
    }

    /**
     * 删除奖品
     */
    @PreAuthorize("@ss.hasPermi('system:prize:remove')")
    @Log(title = "奖品", businessType = BusinessType.DELETE)
	@DeleteMapping("/{articleIds}")
    public AjaxResult remove(@PathVariable String[] articleIds)
    {
        return toAjax(catPrizeService.deleteCatPrizeByArticleIds(articleIds));
    }

}
