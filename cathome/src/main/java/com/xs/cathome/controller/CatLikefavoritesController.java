package com.xs.cathome.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.xs.cathome.domain.CatLikefavorites;
import com.xs.cathome.service.ICatLikefavoritesService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 点赞收藏Controller
 * 
 * @author ruoyi
 * @date 2023-03-14
 */
@RestController
@RequestMapping("/cathome/likefavorites")
public class CatLikefavoritesController extends BaseController
{
    @Autowired
    private ICatLikefavoritesService catLikefavoritesService;

    /**
     * 查询点赞收藏列表
     */
    @PreAuthorize("@ss.hasPermi('system:likefavorites:list')")
    @GetMapping("/list")
    public TableDataInfo list(CatLikefavorites catLikefavorites)
    {
        startPage();
        List<CatLikefavorites> list = catLikefavoritesService.selectCatLikefavoritesList(catLikefavorites);
        return getDataTable(list);
    }

    /**
     * 导出点赞收藏列表
     */
    @PreAuthorize("@ss.hasPermi('system:likefavorites:export')")
    @Log(title = "点赞收藏", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CatLikefavorites catLikefavorites)
    {
        List<CatLikefavorites> list = catLikefavoritesService.selectCatLikefavoritesList(catLikefavorites);
        ExcelUtil<CatLikefavorites> util = new ExcelUtil<CatLikefavorites>(CatLikefavorites.class);
        util.exportExcel(response, list, "点赞收藏数据");
    }

    /**
     * 获取点赞收藏详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:likefavorites:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(catLikefavoritesService.selectCatLikefavoritesById(id));
    }

    /**
     * 新增点赞收藏
     */
    @PreAuthorize("@ss.hasPermi('system:likefavorites:add')")
    @Log(title = "点赞收藏", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CatLikefavorites catLikefavorites)
    {
        return toAjax(catLikefavoritesService.insertCatLikefavorites(catLikefavorites));
    }

    /**
     * 修改点赞收藏
     */
    @PreAuthorize("@ss.hasPermi('system:likefavorites:edit')")
    @Log(title = "点赞收藏", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CatLikefavorites catLikefavorites)
    {
        return toAjax(catLikefavoritesService.updateCatLikefavorites(catLikefavorites));
    }

    /**
     * 删除点赞收藏
     */
    @PreAuthorize("@ss.hasPermi('system:likefavorites:remove')")
    @Log(title = "点赞收藏", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(catLikefavoritesService.deleteCatLikefavoritesByIds(ids));
    }
}
