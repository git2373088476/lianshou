package com.xs.cathome.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.xs.cathome.domain.CatFleaArticleMarket;
import com.xs.cathome.service.ICatFleaArticleMarketService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 跳蚤信息关联表Controller
 * 
 * @author ruoyi
 * @date 2023-03-16
 */
@RestController
@RequestMapping("/system/articleMarket")
public class CatFleaArticleMarketController extends BaseController
{
    @Autowired
    private ICatFleaArticleMarketService catFleaArticleMarketService;

    /**
     * 查询跳蚤信息关联表列表
     */
    @PreAuthorize("@ss.hasPermi('system:market:list')")
    @GetMapping("/list")
    public TableDataInfo list(CatFleaArticleMarket catFleaArticleMarket)
    {
        startPage();
        List<CatFleaArticleMarket> list = catFleaArticleMarketService.selectCatFleaArticleMarketList(catFleaArticleMarket);
        return getDataTable(list);
    }

    /**
     * 导出跳蚤信息关联表列表
     */
    @PreAuthorize("@ss.hasPermi('system:market:export')")
    @Log(title = "跳蚤信息关联表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CatFleaArticleMarket catFleaArticleMarket)
    {
        List<CatFleaArticleMarket> list = catFleaArticleMarketService.selectCatFleaArticleMarketList(catFleaArticleMarket);
        ExcelUtil<CatFleaArticleMarket> util = new ExcelUtil<CatFleaArticleMarket>(CatFleaArticleMarket.class);
        util.exportExcel(response, list, "跳蚤信息关联表数据");
    }

    /**
     * 获取跳蚤信息关联表详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:market:query')")
    @GetMapping(value = "/{artMarId}")
    public AjaxResult getInfo(@PathVariable("artMarId") String artMarId)
    {
        return success(catFleaArticleMarketService.selectCatFleaArticleMarketByArtMarId(artMarId));
    }

    /**
     * 新增跳蚤信息关联表
     */
    @PreAuthorize("@ss.hasPermi('system:market:add')")
    @Log(title = "跳蚤信息关联表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CatFleaArticleMarket catFleaArticleMarket)
    {
        return toAjax(catFleaArticleMarketService.insertCatFleaArticleMarket(catFleaArticleMarket));
    }

    /**
     * 修改跳蚤信息关联表
     */
    @PreAuthorize("@ss.hasPermi('system:market:edit')")
    @Log(title = "跳蚤信息关联表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CatFleaArticleMarket catFleaArticleMarket)
    {
        return toAjax(catFleaArticleMarketService.updateCatFleaArticleMarket(catFleaArticleMarket));
    }

    /**
     * 删除跳蚤信息关联表
     */
    @PreAuthorize("@ss.hasPermi('system:market:remove')")
    @Log(title = "跳蚤信息关联表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{artMarIds}")
    public AjaxResult remove(@PathVariable String[] artMarIds)
    {
        return toAjax(catFleaArticleMarketService.deleteCatFleaArticleMarketByArtMarIds(artMarIds));
    }
}
