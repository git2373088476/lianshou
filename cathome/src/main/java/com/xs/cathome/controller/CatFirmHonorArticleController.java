package com.xs.cathome.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.xs.cathome.domain.CatFirmHonorArticle;
import com.xs.cathome.service.ICatFirmHonorArticleService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 公司荣誉和评论Controller
 * 
 * @author ruoyi
 * @date 2023-03-15
 */
@RestController
@RequestMapping("/system/firmarticle")
public class CatFirmHonorArticleController extends BaseController
{
    @Autowired
    private ICatFirmHonorArticleService catFirmHonorArticleService;

    /**
     * 查询公司荣誉和评论列表
     */
    @PreAuthorize("@ss.hasPermi('system:article:list')")
    @GetMapping("/list")
    public TableDataInfo list(CatFirmHonorArticle catFirmHonorArticle)
    {
        startPage();
        List<CatFirmHonorArticle> list = catFirmHonorArticleService.selectCatFirmHonorArticleList(catFirmHonorArticle);
        return getDataTable(list);
    }

    /**
     * 导出公司荣誉和评论列表
     */
    @PreAuthorize("@ss.hasPermi('system:article:export')")
    @Log(title = "公司荣誉和评论", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CatFirmHonorArticle catFirmHonorArticle)
    {
        List<CatFirmHonorArticle> list = catFirmHonorArticleService.selectCatFirmHonorArticleList(catFirmHonorArticle);
        ExcelUtil<CatFirmHonorArticle> util = new ExcelUtil<CatFirmHonorArticle>(CatFirmHonorArticle.class);
        util.exportExcel(response, list, "公司荣誉和评论数据");
    }

    /**
     * 获取公司荣誉和评论详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:article:query')")
    @GetMapping(value = "/{artHonorId}")
    public AjaxResult getInfo(@PathVariable("artHonorId") String artHonorId)
    {
        return success(catFirmHonorArticleService.selectCatFirmHonorArticleByArtHonorId(artHonorId));
    }

    /**
     * 新增公司荣誉和评论
     */
    @PreAuthorize("@ss.hasPermi('system:article:add')")
    @Log(title = "公司荣誉和评论", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CatFirmHonorArticle catFirmHonorArticle)
    {
        return toAjax(catFirmHonorArticleService.insertCatFirmHonorArticle(catFirmHonorArticle));
    }

    /**
     * 修改公司荣誉和评论
     */
    @PreAuthorize("@ss.hasPermi('system:article:edit')")
    @Log(title = "公司荣誉和评论", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CatFirmHonorArticle catFirmHonorArticle)
    {
        return toAjax(catFirmHonorArticleService.updateCatFirmHonorArticle(catFirmHonorArticle));
    }

    /**
     * 删除公司荣誉和评论
     */
    @PreAuthorize("@ss.hasPermi('system:article:remove')")
    @Log(title = "公司荣誉和评论", businessType = BusinessType.DELETE)
	@DeleteMapping("/{artHonorIds}")
    public AjaxResult remove(@PathVariable String[] artHonorIds)
    {
        return toAjax(catFirmHonorArticleService.deleteCatFirmHonorArticleByArtHonorIds(artHonorIds));
    }
}
