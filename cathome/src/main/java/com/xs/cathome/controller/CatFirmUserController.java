package com.xs.cathome.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.xs.cathome.domain.CatFirmUser;
import com.xs.cathome.service.ICatFirmUserService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 公司员工表Controller
 * 
 * @author ruoyi
 * @date 2023-03-15
 */
@RestController
@RequestMapping("/system/firmuser")
public class CatFirmUserController extends BaseController
{
    @Autowired
    private ICatFirmUserService catFirmUserService;

    /**
     * 查询公司员工表列表
     */
    @PreAuthorize("@ss.hasPermi('system:user:list')")
    @GetMapping("/list")
    public TableDataInfo list(CatFirmUser catFirmUser)
    {
        startPage();
        List<CatFirmUser> list = catFirmUserService.selectCatFirmUserList(catFirmUser);
        return getDataTable(list);
    }

    /**
     * 导出公司员工表列表
     */
    @PreAuthorize("@ss.hasPermi('system:user:export')")
    @Log(title = "公司员工表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CatFirmUser catFirmUser)
    {
        List<CatFirmUser> list = catFirmUserService.selectCatFirmUserList(catFirmUser);
        ExcelUtil<CatFirmUser> util = new ExcelUtil<CatFirmUser>(CatFirmUser.class);
        util.exportExcel(response, list, "公司员工表数据");
    }

    /**
     * 获取公司员工表详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:user:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return success(catFirmUserService.selectCatFirmUserById(id));
    }

    /**
     * 新增公司员工表
     */
    @PreAuthorize("@ss.hasPermi('system:user:add')")
    @Log(title = "公司员工表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CatFirmUser catFirmUser)
    {
        return toAjax(catFirmUserService.insertCatFirmUser(catFirmUser));
    }

    /**
     * 修改公司员工表
     */
    @PreAuthorize("@ss.hasPermi('system:user:edit')")
    @Log(title = "公司员工表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CatFirmUser catFirmUser)
    {
        return toAjax(catFirmUserService.updateCatFirmUser(catFirmUser));
    }

    /**
     * 删除公司员工表
     */
    @PreAuthorize("@ss.hasPermi('system:user:remove')")
    @Log(title = "公司员工表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(catFirmUserService.deleteCatFirmUserByIds(ids));
    }
}
