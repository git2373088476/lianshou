package com.xs.cathome.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.xs.cathome.domain.CatTipCollection;
import com.xs.cathome.service.ICatTipCollectionService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 打赏收藏Controller
 * 
 * @author ruoyi
 * @date 2023-03-14
 */
@RestController
@RequestMapping("/system/collection")
public class CatTipCollectionController extends BaseController
{
    @Autowired
    private ICatTipCollectionService catTipCollectionService;

    /**
     * 查询打赏收藏列表
     */
    @PreAuthorize("@ss.hasPermi('system:collection:list')")
    @GetMapping("/list")
    public TableDataInfo list(CatTipCollection catTipCollection)
    {
        startPage();
        List<CatTipCollection> list = catTipCollectionService.selectCatTipCollectionList(catTipCollection);
        return getDataTable(list);
    }

    /**
     * 导出打赏收藏列表
     */
    @PreAuthorize("@ss.hasPermi('system:collection:export')")
    @Log(title = "打赏收藏", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CatTipCollection catTipCollection)
    {
        List<CatTipCollection> list = catTipCollectionService.selectCatTipCollectionList(catTipCollection);
        ExcelUtil<CatTipCollection> util = new ExcelUtil<CatTipCollection>(CatTipCollection.class);
        util.exportExcel(response, list, "打赏收藏数据");
    }

    /**
     * 获取打赏收藏详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:collection:query')")
    @GetMapping(value = "/{collectionId}")
    public AjaxResult getInfo(@PathVariable("collectionId") String collectionId)
    {
        return success(catTipCollectionService.selectCatTipCollectionByCollectionId(collectionId));
    }

    /**
     * 新增打赏收藏
     */
    @PreAuthorize("@ss.hasPermi('system:collection:add')")
    @Log(title = "打赏收藏", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CatTipCollection catTipCollection)
    {
        return toAjax(catTipCollectionService.insertCatTipCollection(catTipCollection));
    }

    /**
     * 修改打赏收藏
     */
    @PreAuthorize("@ss.hasPermi('system:collection:edit')")
    @Log(title = "打赏收藏", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CatTipCollection catTipCollection)
    {
        return toAjax(catTipCollectionService.updateCatTipCollection(catTipCollection));
    }

    /**
     * 删除打赏收藏
     */
    @PreAuthorize("@ss.hasPermi('system:collection:remove')")
    @Log(title = "打赏收藏", businessType = BusinessType.DELETE)
	@DeleteMapping("/{collectionIds}")
    public AjaxResult remove(@PathVariable String[] collectionIds)
    {
        return toAjax(catTipCollectionService.deleteCatTipCollectionByCollectionIds(collectionIds));
    }
}
