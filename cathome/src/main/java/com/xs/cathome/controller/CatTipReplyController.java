package com.xs.cathome.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.xs.cathome.domain.vo.CatUserAndTipRepliesVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.xs.cathome.domain.CatTipReply;
import com.xs.cathome.service.ICatTipReplyService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 打赏评论Controller
 * 
 * @author ruoyi
 * @date 2023-03-14
 */
@RestController
@RequestMapping("/system/reply")
public class CatTipReplyController extends BaseController
{
    @Autowired
    private ICatTipReplyService catTipReplyService;

    /**
     * 查询打赏评论列表
     */
    @PreAuthorize("@ss.hasPermi('system:reply:list')")
    @GetMapping("/tipReplylist/{articleId}")
    public TableDataInfo tipReplylist(@PathVariable("articleId") String articleId)
    {
        List<CatUserAndTipRepliesVO> list = catTipReplyService.selectTipReplyList(articleId);
        System.out.println("***********");
        System.out.println(list);
        return getDataTable(list);
    }

    /**
     * 查询打赏评论列表
     */
    @PreAuthorize("@ss.hasPermi('system:reply:list')")
    @GetMapping("/list")
    public TableDataInfo list(CatTipReply catTipReply)
    {
        startPage();
        List<CatTipReply> list = catTipReplyService.selectCatTipReplyList(catTipReply);
        return getDataTable(list);
    }

    /**
     * 导出打赏评论列表
     */
    @PreAuthorize("@ss.hasPermi('system:reply:export')")
    @Log(title = "打赏评论", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CatTipReply catTipReply)
    {
        List<CatTipReply> list = catTipReplyService.selectCatTipReplyList(catTipReply);
        ExcelUtil<CatTipReply> util = new ExcelUtil<CatTipReply>(CatTipReply.class);
        util.exportExcel(response, list, "打赏评论数据");
    }

    /**
     * 获取打赏评论详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:reply:query')")
    @GetMapping(value = "/{userId}")
    public AjaxResult getInfo(@PathVariable("userId") String userId)
    {
        return success(catTipReplyService.selectCatTipReplyByUserId(userId));
    }

    /**
     * 新增打赏评论
     */
    @PreAuthorize("@ss.hasPermi('system:reply:add')")
    @Log(title = "打赏评论", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody CatTipReply catTipReply)
    {
        return toAjax(catTipReplyService.insertCatTipReply(catTipReply));
    }

    /**
     * 修改打赏评论
     */
    @PreAuthorize("@ss.hasPermi('system:reply:edit')")
    @Log(title = "打赏评论", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CatTipReply catTipReply)
    {
        return toAjax(catTipReplyService.updateCatTipReply(catTipReply));
    }

    /**
     * 删除打赏评论
     */
    @PreAuthorize("@ss.hasPermi('system:reply:remove')")
    @Log(title = "打赏评论", businessType = BusinessType.DELETE)
	@DeleteMapping("/{userIds}")
    public AjaxResult remove(@PathVariable String[] userIds)
    {
        return toAjax(catTipReplyService.deleteCatTipReplyByUserIds(userIds));
    }
}
