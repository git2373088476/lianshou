package com.xs.cathome.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.xs.cathome.domain.CatComment;
import com.xs.cathome.service.ICatCommentService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 评论Controller
 * 
 * @author ruoyi
 * @date 2023-03-14
 */
@CrossOrigin
@RestController
@RequestMapping("/cathome/comment")
public class CatCommentController extends BaseController
{
    @Autowired
    private ICatCommentService catCommentService;

    /**
     * 查询评论列表
     */
    @PreAuthorize("@ss.hasPermi('cathome:comment:list')")
    @GetMapping("/list")
    public TableDataInfo list(CatComment catComment)
    {
        startPage();
        List<CatComment> list = catCommentService.selectCatCommentList(catComment);
        return getDataTable(list);
    }

    /**
     * 导出评论列表
     */
    @PreAuthorize("@ss.hasPermi('system:comment:export')")
    @Log(title = "评论", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CatComment catComment)
    {
        List<CatComment> list = catCommentService.selectCatCommentList(catComment);
        ExcelUtil<CatComment> util = new ExcelUtil<CatComment>(CatComment.class);
        util.exportExcel(response, list, "评论数据");
    }

    /**
     * 获取评论详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:comment:query')")
    @GetMapping(value = "/{commentId}")
    public AjaxResult getInfo(@PathVariable("commentId") String commentId)
    {
        return success(catCommentService.selectCatCommentByCommentId(commentId));
    }

    /**
     * 新增评论
     */
    @PreAuthorize("@ss.hasPermi('system:comment:add')")
    @Log(title = "评论", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CatComment catComment)
    {
        return toAjax(catCommentService.insertCatComment(catComment));
    }

    /**
     * 修改评论
     */
    @PreAuthorize("@ss.hasPermi('system:comment:edit')")
    @Log(title = "评论", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CatComment catComment)
    {
        return toAjax(catCommentService.updateCatComment(catComment));
    }

    /**
     * 删除评论
     */
    @PreAuthorize("@ss.hasPermi('system:comment:remove')")
    @Log(title = "评论", businessType = BusinessType.DELETE)
	@DeleteMapping("/{commentIds}")
    public AjaxResult remove(@PathVariable String[] commentIds)
    {
        return toAjax(catCommentService.deleteCatCommentByCommentIds(commentIds));
    }
}
