package com.xs.cathome.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.xs.cathome.domain.vo.CatFleaMarketVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.xs.cathome.domain.CatFleaMarket;
import com.xs.cathome.service.ICatFleaMarketService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 跳蚤市场Controller
 * 
 * @author ruoyi
 * @date 2023-03-16
 */
@RestController
@RequestMapping("/system/market")
public class CatFleaMarketController extends BaseController
{
    @Autowired
    private ICatFleaMarketService catFleaMarketService;

    /**
     * 查询所有列表
     */
    @PreAuthorize("@ss.hasPermi('system:market:marketList')")
    @GetMapping("/marketList")
    public TableDataInfo marketList(CatFleaMarketVO catFleaMarketVO)
    {
        startPage();
        List<CatFleaMarketVO> list = catFleaMarketService.selectMarketList(catFleaMarketVO);
        return getDataTable(list);
    }

    /**
     * 新增
     */
    @PreAuthorize("@ss.hasPermi('system:market:addmarket')")
    @Log(title = "跳蚤市场", businessType = BusinessType.INSERT)
    @PostMapping("/addmarket")
    public AjaxResult addmarket(@RequestBody CatFleaMarketVO catFleaMarketVO)
    {
        return toAjax(catFleaMarketService.addmarket(catFleaMarketVO));
    }

    /**
     * 查询跳蚤市场列表
     */
    @PreAuthorize("@ss.hasPermi('system:market:list')")
    @GetMapping("/list")
    public TableDataInfo list(CatFleaMarket catFleaMarket)
    {
        startPage();
        List<CatFleaMarket> list = catFleaMarketService.selectCatFleaMarketList(catFleaMarket);
        return getDataTable(list);
    }

    /**
     * 导出跳蚤市场列表
     */
    @PreAuthorize("@ss.hasPermi('system:market:export')")
    @Log(title = "跳蚤市场", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CatFleaMarket catFleaMarket)
    {
        List<CatFleaMarket> list = catFleaMarketService.selectCatFleaMarketList(catFleaMarket);
        ExcelUtil<CatFleaMarket> util = new ExcelUtil<CatFleaMarket>(CatFleaMarket.class);
        util.exportExcel(response, list, "跳蚤市场数据");
    }

    /**
     * 获取跳蚤市场详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:market:query')")
    @GetMapping(value = "/{marketId}")
    public AjaxResult getInfo(@PathVariable("marketId") String marketId)
    {
        return success(catFleaMarketService.selectCatFleaMarketByMarketId(marketId));
    }

    /**
     * 新增跳蚤市场
     */
    @PreAuthorize("@ss.hasPermi('system:market:add')")
    @Log(title = "跳蚤市场", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CatFleaMarket catFleaMarket)
    {
        return toAjax(catFleaMarketService.insertCatFleaMarket(catFleaMarket));
    }

    /**
     * 修改跳蚤市场
     */
    @PreAuthorize("@ss.hasPermi('system:market:edit')")
    @Log(title = "跳蚤市场", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CatFleaMarket catFleaMarket)
    {
        return toAjax(catFleaMarketService.updateCatFleaMarket(catFleaMarket));
    }

    /**
     * 删除跳蚤市场
     */
    @PreAuthorize("@ss.hasPermi('system:market:remove')")
    @Log(title = "跳蚤市场", businessType = BusinessType.DELETE)
	@DeleteMapping("/{marketIds}")
    public AjaxResult remove(@PathVariable String[] marketIds)
    {
        return toAjax(catFleaMarketService.deleteCatFleaMarketByMarketIds(marketIds));
    }
}
