package com.xs.cathome.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.xs.cathome.domain.CatTipArticle;
import com.xs.cathome.service.ICatTipArticleService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 打赏发布表Controller
 * 
 * @author ruoyi
 * @date 2023-03-14
 */
@RestController
@RequestMapping("/system/article")
public class CatTipArticleController extends BaseController
{
    @Autowired
    private ICatTipArticleService catTipArticleService;

    /**
     * 按照最新、推荐、最热查询打赏发布列表
     * @param status 状态
     * @return
     */
    @GetMapping("/listStatus/{status}")
    public TableDataInfo listStatus(@PathVariable(value = "status") String status){
        startPage();
        List<CatTipArticle> list = catTipArticleService.selectCatTipArticleList(status);
        return getDataTable(list);
    }

    /**
     * 查询打赏发布表列表
     */
    @PreAuthorize("@ss.hasPermi('system:article:list')")
    @GetMapping("/list")
    public TableDataInfo list(CatTipArticle catTipArticle)
    {
        startPage();
        List<CatTipArticle> list = catTipArticleService.selectCatTipArticleList(catTipArticle);
        return getDataTable(list);
    }

    /**
     * 导出打赏发布表列表
     */
    @PreAuthorize("@ss.hasPermi('system:article:export')")
    @Log(title = "打赏发布表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CatTipArticle catTipArticle)
    {
        List<CatTipArticle> list = catTipArticleService.selectCatTipArticleList(catTipArticle);
        ExcelUtil<CatTipArticle> util = new ExcelUtil<CatTipArticle>(CatTipArticle.class);
        util.exportExcel(response, list, "打赏发布表数据");
    }

    /**
     * 获取打赏发布表详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:article:query')")
    @GetMapping(value = "/{articleId}")
    public AjaxResult getInfo(@PathVariable("articleId") String articleId)
    {
        return success(catTipArticleService.selectCatTipArticleByArticleId(articleId));
    }

    /**
     * 新增打赏发布表
     */
    @PreAuthorize("@ss.hasPermi('system:article:add')")
    @Log(title = "打赏发布表", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody CatTipArticle catTipArticle)
    {
//        System.out.println("add**********");
        return toAjax(catTipArticleService.insertCatTipArticle(catTipArticle));
    }

    /**
     * 修改打赏发布表
     */
    @PreAuthorize("@ss.hasPermi('system:article:edit')")
    @Log(title = "打赏发布表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CatTipArticle catTipArticle)
    {
        return toAjax(catTipArticleService.updateCatTipArticle(catTipArticle));
    }

    /**
     * 删除打赏发布表
     */
    @PreAuthorize("@ss.hasPermi('system:article:remove')")
    @Log(title = "打赏发布表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{articleIds}")
    public AjaxResult remove(@PathVariable String[] articleIds)
    {
        return toAjax(catTipArticleService.deleteCatTipArticleByArticleIds(articleIds));
    }
}
