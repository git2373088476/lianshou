package com.xs.cathome.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.xs.cathome.domain.CatFirm;
import com.xs.cathome.service.ICatFirmService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 公司表Controller
 * 
 * @author ruoyi
 * @date 2023-03-15
 */
@RestController
@RequestMapping("/system/firm")
public class CatFirmController extends BaseController
{
    @Autowired
    private ICatFirmService catFirmService;

    /**
     * 查询公司表列表
     */
    @PreAuthorize("@ss.hasPermi('system:firm:list')")
    @GetMapping("/list")
    public TableDataInfo list(CatFirm catFirm)
    {
        startPage();
        List<CatFirm> list = catFirmService.selectCatFirmList(catFirm);
        return getDataTable(list);
    }

    /**
     * 导出公司表列表
     */
    @PreAuthorize("@ss.hasPermi('system:firm:export')")
    @Log(title = "公司表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CatFirm catFirm)
    {
        List<CatFirm> list = catFirmService.selectCatFirmList(catFirm);
        ExcelUtil<CatFirm> util = new ExcelUtil<CatFirm>(CatFirm.class);
        util.exportExcel(response, list, "公司表数据");
    }

    /**
     * 获取公司表详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:firm:query')")
    @GetMapping(value = "/{firmId}")
    public AjaxResult getInfo(@PathVariable("firmId") String firmId)
    {
        return success(catFirmService.selectCatFirmByFirmId(firmId));
    }

    /**
     * 新增公司表
     */
    @PreAuthorize("@ss.hasPermi('system:firm:add')")
    @Log(title = "公司表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CatFirm catFirm)
    {
        return toAjax(catFirmService.insertCatFirm(catFirm));
    }

    /**
     * 修改公司表
     */
    @PreAuthorize("@ss.hasPermi('system:firm:edit')")
    @Log(title = "公司表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CatFirm catFirm)
    {
        return toAjax(catFirmService.updateCatFirm(catFirm));
    }

    /**
     * 删除公司表
     */
    @PreAuthorize("@ss.hasPermi('system:firm:remove')")
    @Log(title = "公司表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{firmIds}")
    public AjaxResult remove(@PathVariable String[] firmIds)
    {
        return toAjax(catFirmService.deleteCatFirmByFirmIds(firmIds));
    }
}
