package com.xs.cathome.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.xs.cathome.domain.CatTipLike;
import com.xs.cathome.service.ICatTipLikeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 打赏点赞Controller
 * 
 * @author ruoyi
 * @date 2023-03-14
 */
@RestController
@RequestMapping("/system/like")
public class CatTipLikeController extends BaseController
{
    @Autowired
    private ICatTipLikeService catTipLikeService;

    /**
     * 查询打赏点赞列表
     */
    @PreAuthorize("@ss.hasPermi('system:like:list')")
    @GetMapping("/list")
    public TableDataInfo list(CatTipLike catTipLike)
    {
        startPage();
        List<CatTipLike> list = catTipLikeService.selectCatTipLikeList(catTipLike);
        return getDataTable(list);
    }

    /**
     * 导出打赏点赞列表
     */
    @PreAuthorize("@ss.hasPermi('system:like:export')")
    @Log(title = "打赏点赞", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CatTipLike catTipLike)
    {
        List<CatTipLike> list = catTipLikeService.selectCatTipLikeList(catTipLike);
        ExcelUtil<CatTipLike> util = new ExcelUtil<CatTipLike>(CatTipLike.class);
        util.exportExcel(response, list, "打赏点赞数据");
    }

    /**
     * 获取打赏点赞详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:like:query')")
    @GetMapping(value = "/{likeId}")
    public AjaxResult getInfo(@PathVariable("likeId") String likeId)
    {
        return success(catTipLikeService.selectCatTipLikeByLikeId(likeId));
    }

    /**
     * 新增打赏点赞
     */
    @PreAuthorize("@ss.hasPermi('system:like:add')")
    @Log(title = "打赏点赞", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CatTipLike catTipLike)
    {
        return toAjax(catTipLikeService.insertCatTipLike(catTipLike));
    }

    /**
     * 修改打赏点赞
     */
    @PreAuthorize("@ss.hasPermi('system:like:edit')")
    @Log(title = "打赏点赞", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CatTipLike catTipLike)
    {
        return toAjax(catTipLikeService.updateCatTipLike(catTipLike));
    }

    /**
     * 删除打赏点赞
     */
    @PreAuthorize("@ss.hasPermi('system:like:remove')")
    @Log(title = "打赏点赞", businessType = BusinessType.DELETE)
	@DeleteMapping("/{likeIds}")
    public AjaxResult remove(@PathVariable String[] likeIds)
    {
        return toAjax(catTipLikeService.deleteCatTipLikeByLikeIds(likeIds));
    }
}
