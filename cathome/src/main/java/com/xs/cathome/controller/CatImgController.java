package com.xs.cathome.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.xs.cathome.domain.CatImg;
import com.xs.cathome.service.ICatImgService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 文章图片 评论图片Controller
 * 
 * @author ruoyi
 * @date 2023-03-14
 */
@RestController
@RequestMapping("/cathome/img")
public class CatImgController extends BaseController
{
    @Autowired
    private ICatImgService catImgService;

    /**
     * 查询文章图片 评论图片列表
     */
    @PreAuthorize("@ss.hasPermi('system:img:list')")
    @GetMapping("/list")
    public TableDataInfo list(CatImg catImg)
    {
        startPage();
        List<CatImg> list = catImgService.selectCatImgList(catImg);
        return getDataTable(list);
    }

    /**
     * 导出文章图片 评论图片列表
     */
    @PreAuthorize("@ss.hasPermi('system:img:export')")
    @Log(title = "文章图片 评论图片", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CatImg catImg)
    {
        List<CatImg> list = catImgService.selectCatImgList(catImg);
        ExcelUtil<CatImg> util = new ExcelUtil<CatImg>(CatImg.class);
        util.exportExcel(response, list, "文章图片 评论图片数据");
    }

    /**
     * 获取文章图片 评论图片详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:img:query')")
    @GetMapping(value = "/{imgId}")
    public AjaxResult getInfo(@PathVariable("imgId") String imgId)
    {
        return success(catImgService.selectCatImgByImgId(imgId));
    }

    /**
     * 新增文章图片 评论图片
     */
    @PreAuthorize("@ss.hasPermi('system:img:add')")
    @Log(title = "文章图片 评论图片", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CatImg catImg)
    {
        return toAjax(catImgService.insertCatImg(catImg));
    }

    /**
     * 修改文章图片 评论图片
     */
    @PreAuthorize("@ss.hasPermi('system:img:edit')")
    @Log(title = "文章图片 评论图片", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CatImg catImg)
    {
        return toAjax(catImgService.updateCatImg(catImg));
    }

    /**
     * 删除文章图片 评论图片
     */
    @PreAuthorize("@ss.hasPermi('system:img:remove')")
    @Log(title = "文章图片 评论图片", businessType = BusinessType.DELETE)
	@DeleteMapping("/{imgIds}")
    public AjaxResult remove(@PathVariable String[] imgIds)
    {
        return toAjax(catImgService.deleteCatImgByImgIds(imgIds));
    }
}
