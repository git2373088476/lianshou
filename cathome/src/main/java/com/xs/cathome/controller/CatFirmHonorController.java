package com.xs.cathome.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.xs.cathome.domain.vo.CatFirmUserVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.xs.cathome.domain.CatFirmHonor;
import com.xs.cathome.service.ICatFirmHonorService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 公司荣誉表Controller
 * 
 * @author ruoyi
 * @date 2023-03-15
 */
@RestController
@RequestMapping("/system/firmhonor")
public class CatFirmHonorController extends BaseController
{
    @Autowired
    private ICatFirmHonorService catFirmHonorService;

    /**
     * 查询公司荣誉表列表
     */
    @PreAuthorize("@ss.hasPermi('system:honor:listhonor')")
    @GetMapping("/listhonor/{firmId}")
    public TableDataInfo listhonor(@PathVariable("firmId") String firmId)
    {
        startPage();
        List<CatFirmUserVO> list = catFirmHonorService.listhonor(firmId);
        return getDataTable(list);
    }

    /**
     * 新增公司荣誉
     */
    @PreAuthorize("@ss.hasPermi('system:honor:addhonor')")
    @Log(title = "公司荣誉表", businessType = BusinessType.INSERT)
    @PostMapping("/addhonor")
    public AjaxResult addhonor(@RequestBody CatFirmUserVO catFirmUserVO)
    {
        return toAjax(catFirmHonorService.addhonor(catFirmUserVO));
    }

    /**
     * 查询公司荣誉表列表
     */
    @PreAuthorize("@ss.hasPermi('system:honor:list')")
    @GetMapping("/list")
    public TableDataInfo list(CatFirmHonor catFirmHonor)
    {
        startPage();
        List<CatFirmHonor> list = catFirmHonorService.selectCatFirmHonorList(catFirmHonor);
        return getDataTable(list);
    }

    /**
     * 导出公司荣誉表列表
     */
    @PreAuthorize("@ss.hasPermi('system:honor:export')")
    @Log(title = "公司荣誉表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CatFirmHonor catFirmHonor)
    {
        List<CatFirmHonor> list = catFirmHonorService.selectCatFirmHonorList(catFirmHonor);
        ExcelUtil<CatFirmHonor> util = new ExcelUtil<CatFirmHonor>(CatFirmHonor.class);
        util.exportExcel(response, list, "公司荣誉表数据");
    }

    /**
     * 获取公司荣誉表详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:honor:query')")
    @GetMapping(value = "/{honorId}")
    public AjaxResult getInfo(@PathVariable("honorId") String honorId)
    {
        return success(catFirmHonorService.selectCatFirmHonorByHonorId(honorId));
    }

    /**
     * 新增公司荣誉表
     */
    @PreAuthorize("@ss.hasPermi('system:honor:add')")
    @Log(title = "公司荣誉表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CatFirmHonor catFirmHonor)
    {
        return toAjax(catFirmHonorService.insertCatFirmHonor(catFirmHonor));
    }

    /**
     * 修改公司荣誉表
     */
    @PreAuthorize("@ss.hasPermi('system:honor:edit')")
    @Log(title = "公司荣誉表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CatFirmHonor catFirmHonor)
    {
        return toAjax(catFirmHonorService.updateCatFirmHonor(catFirmHonor));
    }

    /**
     * 删除公司荣誉表
     */
    @PreAuthorize("@ss.hasPermi('system:honor:remove')")
    @Log(title = "公司荣誉表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{honorIds}")
    public AjaxResult remove(@PathVariable String[] honorIds)
    {
        return toAjax(catFirmHonorService.deleteCatFirmHonorByHonorIds(honorIds));
    }
}
